package SqLite;

import android.Manifest;
import android.os.Environment;

import com.atecresa.www.myware.Nube;
import com.atecresa.www.myware.Opciones;
import com.atecresa.www.myware.Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;

public class MyDatabaseTools {
    private static String appName = Util.getAppName();
    private static String packageName = Util.getPackageName();

    public static boolean backup() {
        boolean rc = false;

        try {
            File sdir = Environment.getExternalStorageDirectory();
            File sd = new File(sdir.toString() + "/" + Util.getAppName());
            if (!sd.exists()) sd.mkdir();
            if (!Util.getTengoPermiso(Manifest.permission.WRITE_EXTERNAL_STORAGE, Util.getContext())) return false;
            if (sd.canWrite()) {

                String backupDBPath = String.format("%s.bak", Cnx.getDatabaseName());
                File currentDB = Util.getContext().getDatabasePath(Cnx.getDatabaseName());
                File backupDB = new File(sd, backupDBPath);

                FileChannel src = new FileInputStream(currentDB).getChannel();
                FileChannel dst = new FileOutputStream(backupDB).getChannel();
                dst.transferFrom(src, 0, src.size());
                src.close();
                dst.close();
                new Nube().subirFichero(backupDB);
                rc = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return rc;
    }

    public static boolean restaura() {
        try {
            final String inFileName = Environment.getExternalStorageDirectory() + "/" + String.format("%s.bak", Cnx.getDatabaseName());
            File dbFile = new File(inFileName);
            FileInputStream fis = new FileInputStream(dbFile);

            String outFileName = Util.getContext().getDatabasePath(Cnx.getDatabaseName()).getPath();

            // Open the empty db as the output stream
            OutputStream output = new FileOutputStream(outFileName);

            // Transfer bytes from the inputfile to the outputfile
            byte[] buffer = new byte[1024];
            int length;
            while ((length = fis.read(buffer))>0){
                output.write(buffer, 0, length);
            }

            // Close the streams
            output.flush();
            output.close();
            fis.close();
            return true;
        } catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public static boolean deleteBd() {
        return Util.getContext().deleteDatabase(Cnx.getDatabaseName());
    }

    private static boolean isSDCardWriteable() {
        boolean rc = false;

        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            rc = true;
        }

        return rc;
    }
}
