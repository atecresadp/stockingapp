package SqLite;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.atecresa.www.myware.Opciones;

public class ConectarBDMyWare extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "DBMyWare.db";
    private static final int miVersion = 3;

    private StringBuilder sqlCreate;

    protected ConectarBDMyWare(Context contexto) {
        super(contexto, DATABASE_NAME, null, miVersion);

        sqlCreate = new StringBuilder();

        sqlCreate.append("CREATE TABLE ALMACENES (");
        sqlCreate.append("ID INTEGER NOT NULL, ");
        sqlCreate.append("CODIGO INTEGER PRIMARY KEY, ");
        sqlCreate.append("NOMBRE TEXT(20) NOT NULL);");

        sqlCreate.append("CREATE TABLE ARTICULOS (");
        sqlCreate.append("ID INTEGER PRIMARY KEY AUTOINCREMENT, ");
        sqlCreate.append("CODIGO TEXT(32) NOT NULL, ");
        sqlCreate.append("NOMBRE TEXT(20) NOT NULL, ");
        sqlCreate.append("TIPO TEXT(1) NOT NULL DEFAULT 'X', ");
        sqlCreate.append("IDFAMILIA INTEGER NOT NULL DEFAULT 1, ");
        sqlCreate.append("PVP REAL NOT NULL DEFAULT 0, ");
        sqlCreate.append("COSTO REAL NOT NULL DEFAULT 0, ");
        sqlCreate.append("TOCADO INT NOT NULL DEFAULT 0);");

        sqlCreate.append("CREATE UNIQUE INDEX IDX_CODARTICULO ON ARTICULOS (CODIGO ASC);");

        sqlCreate.append("CREATE TABLE RELACIONES (");
        sqlCreate.append("ID INTEGER NOT NULL, ");
        sqlCreate.append("IDARTICULO INTEGER NOT NULL, ");
        sqlCreate.append("CODEXTERNO TEXT(32) NOT NULL, ");
        sqlCreate.append("IDPROVEEDOR INTEGER NOT NULL DEFAULT -1, ");
        sqlCreate.append("PRIMARY KEY (IDARTICULO, CODEXTERNO, IDPROVEEDOR));");

        sqlCreate.append("CREATE TABLE CADUCIDAD (");
        sqlCreate.append("ID INTEGER PRIMARY KEY AUTOINCREMENT, ");
        sqlCreate.append("IDARTICULO INTEGER NOT NULL, ");
        sqlCreate.append("FECHACADUCIDAD DATETIME DEFAULT CURRENT_TIME, ");
        sqlCreate.append("NLOTE TEXT(50) NOT NULL DEFAULT '000', ");
        sqlCreate.append("TOCADO INTEGER DEFAULT 0);");

        sqlCreate.append("CREATE TABLE CLIPROV (");
        sqlCreate.append("ID INTEGER NOT NULL, ");
        sqlCreate.append("CODIGO TEXT(13) NOT NULL, ");
        sqlCreate.append("NOMBRE TEXT(20) NOT NULL DEFAULT ' ', ");
        sqlCreate.append("TIPO TEXT(1) NOT NULL DEFAULT 'C', ");
        sqlCreate.append("PRIMARY KEY (CODIGO, TIPO));");

        sqlCreate.append("CREATE TABLE DETALLE (");
        sqlCreate.append("IDMAESTRO INTEGER NOT NULL, ");
        sqlCreate.append("IDARTICULO INTEGER NOT NULL, ");
        sqlCreate.append("UNID REAL NOT NULL DEFAULT 1, ");
        sqlCreate.append("IDCADUCIDAD INTEGER NOT NULL DEFAULT -1, ");
        sqlCreate.append("FECHA DATETIME DEFAULT CURRENT_TIME, ");
        sqlCreate.append("ID INTEGER PRIMARY KEY AUTOINCREMENT);");

        sqlCreate.append("CREATE TABLE DET_OBS (");
        sqlCreate.append("ID INTEGER PRIMARY KEY AUTOINCREMENT, ");
        sqlCreate.append("IDDETALLE INTEGER NOT NULL, ");
        sqlCreate.append("OBS TEXT(255) NOT NULL);");

        sqlCreate.append("CREATE UNIQUE INDEX IDX_IDARTICULO ON DET_OBS (IDDETALLE ASC);");

        sqlCreate.append("CREATE TABLE DRUTAS (");
        sqlCreate.append("ID INTEGER NOT NULL, ");
        sqlCreate.append("IDMRUTA INTEGER NOT NULL, ");
        sqlCreate.append("DIA INTEGER NOT NULL DEFAULT 0, ");
        sqlCreate.append("IDCLIPROV INTEGER NOT NULL DEFAULT 1, ");
        sqlCreate.append("ORDEN INTEGER NOT NULL DEFAULT 0, ");
        sqlCreate.append("FECHA DATETIME, ");
        sqlCreate.append("CADA INTEGER NOT NULL DEFAULT 0, ");
        sqlCreate.append("PRIMARY KEY (IDMRUTA, DIA, IDCLIPROV));");

        sqlCreate.append("CREATE TABLE FAMILIAS (");
        sqlCreate.append("ID INTEGER NOT NULL, ");
        sqlCreate.append("NOMBRE TEXT(20) NOT NULL, ");
        sqlCreate.append("NCADUCIDAD INTEGER DEFAULT 0);");

        sqlCreate.append("CREATE TABLE MAESTRO (");
        sqlCreate.append("ID INTEGER PRIMARY KEY AUTOINCREMENT, ");
        sqlCreate.append("NUMERO INTEGER NOT NULL, ");
        sqlCreate.append("UUID TEXT(255) NOT NULL DEFAULT '', ");
        sqlCreate.append("FECHA DATETIME DEFAULT CURRENT_TIME, ");
        sqlCreate.append("TIPO INTEGER NOT NULL DEFAULT 5, ");
        sqlCreate.append("IDCLIPROV INTEGER NOT NULL DEFAULT 1, ");
        sqlCreate.append("IDALM INTEGER NOT NULL DEFAULT 1, ");
        sqlCreate.append("IDALMDEST INTEGER DEFAULT 0, ");
        sqlCreate.append("OBS TEXT(255) NOT NULL DEFAULT '', ");
        sqlCreate.append("TOCADO INT NOT NULL DEFAULT 1);");

        sqlCreate.append("CREATE UNIQUE INDEX IDX_MAESTRO ON MAESTRO (NUMERO ASC, TIPO ASC, UUID ASC);");

        sqlCreate.append("CREATE TABLE MRUTAS (");
        sqlCreate.append("ID INTEGER NOT NULL, ");
        sqlCreate.append("IDOPERADOR INTEGER NOT NULL DEFAULT 1, ");
        sqlCreate.append("DESCRIPCION TEXT(100) NOT NULL DEFAULT '');");

        sqlCreate.append("CREATE TABLE TIPO_INC (");
        sqlCreate.append("ID INTEGER NOT NULL, ");
        sqlCreate.append("DESCRIPCION TEXT(255) NOT NULL);");

        sqlCreate.append("CREATE TABLE USUARIOS (");
        sqlCreate.append("ID INTEGER NOT NULL, ");
        sqlCreate.append("NOMBRE TEXT(20) NOT NULL);");

        sqlCreate.append("CREATE TABLE CONTADORES (");
        sqlCreate.append("ID INTEGER NOT NULL, ");
        for (Opciones.TipoDoc nc : Opciones.TipoDoc.values()) {
            sqlCreate.append(nc.getNomCampo() + " INTEGER NOT NULL DEFAULT 1, ");
        }

        sqlCreate.delete(sqlCreate.length() - 2, sqlCreate.length());
        sqlCreate.append(");");

        sqlCreate.append("INSERT INTO CONTADORES (ID) VALUES (1);");

    }

    protected String nombreBD() {
        return DATABASE_NAME;
    }

    /**
     * protected String getDatabaseName() {
     * return this.getDatabaseName();
     * }
     **/

    private void CreaTablas(SQLiteDatabase db) {
        for (String t : sqlCreate.toString().split(";")) {
            try {
                db.execSQL(t);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        CreaTablas(db);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int versionAnterior, int versionNueva) {
        sqlCreate = new StringBuilder();

        if (versionAnterior == 1 && versionNueva > 1) {
            sqlCreate.append("ALTER TABLE MAESTRO ADD COLUMN UUID TEXT(50) NOT NULL DEFAULT '';");
            sqlCreate.append("DROP INDEX IF EXISTS IDX_MAESTRO;");
            sqlCreate.append("CREATE UNIQUE INDEX IDX_MAESTRO ON MAESTRO (NUMERO ASC, TIPO ASC, UUID ASC);");
            sqlCreate.append("UPDATE MAESTRO SET UUID = '").append(Opciones.getUuid()).append("';");
            sqlCreate.append("ALTER TABLE CONTADORES ADD COLUMN NPR INTEGER NOT NULL DEFAULT 1;");
            sqlCreate.append("ALTER TABLE CONTADORES ADD COLUMN UUID TEXT(50) NOT NULL DEFAULT '';");
            sqlCreate.append("UPDATE CONTADORES SET UUID = '").append(Opciones.getUuid()).append("';");
        }

        if (versionNueva > 2){
            sqlCreate.append("ALTER TABLE DETALLE ADD COLUMN FECHA DATETIME DEFAULT NULL /* replace me */;");
            sqlCreate.append("UPDATE DETALLE SET FECHA = CURRENT_TIMESTAMP;");
            sqlCreate.append("PRAGMA writable_schema = on;");
            sqlCreate.append("UPDATE sqlite_master SET sql = replace(sql, 'DEFAULT NULL /* replace me */', 'DEFAULT CURRENT_TIMESTAMP') WHERE type = 'table' AND name = 'DETALLE';");
            sqlCreate.append("PRAGMA writable_schema = off;");
        }

        if (sqlCreate.length()>0){
            CreaTablas(db);
        }
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }
}
