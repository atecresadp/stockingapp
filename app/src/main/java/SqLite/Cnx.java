package SqLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.atecresa.www.myware.R;
import com.atecresa.www.myware.Util;

public class Cnx {
    private static String pifia = "";
    private static ConectarBDMyWare usdbh = null;
    private static Context contexto;

    public static SQLiteDatabase beginTran() {
        SQLiteDatabase db = getUsdbh().getWritableDatabase();
        db.beginTransaction();
        return db;
    }

    public static boolean sqlMasivo(String sql, SQLiteDatabase db) {
        boolean ok = false;
        try {
            if (db != null) db.execSQL(sql);
            ok = true;
        } catch (SQLException e) {
            pifia = e.getMessage();
            Util.miError("SqlMasivo", pifia);
            Util.mostrarToastSimple(Util.getContext().getString(R.string.lb_cnx_error), true);
        } catch (Exception e) {
            pifia = e.getMessage();
            Util.miError("SqlMasivo", pifia);
            Util.mostrarToastSimple(Util.getContext().getString(R.string.lb_cnx_error), true);
        }
        return ok;
    }

    public static void commitTran(SQLiteDatabase db) {
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }


    public static int exeSQLInsert(String tabla, ContentValues valores) {
        int id = -1;
        SQLiteDatabase db = getUsdbh().getWritableDatabase();
        pifia = "";
        if (db != null) {
            try {
                id = (int) db.insert(tabla, null, valores);
                db.close();
            } catch (SQLException e) {
                pifia = e.getMessage();
                Util.miError("exeSQLInsert", pifia);
                Util.mostrarToastSimple(Util.getContext().getString(R.string.lb_cnx_error), true);
            } catch (Exception e) {
                pifia = e.getMessage();
                Util.miError("exeSQLInsert", pifia);
                Util.mostrarToastSimple(Util.getContext().getString(R.string.lb_cnx_error), true);
            }
        }
        return id;
    }

    public static int exeSQLUpd(String tabla, ContentValues valores, int idRegistro) {
        int cont = 0;
        String[] criterio = {String.valueOf(idRegistro)};
        SQLiteDatabase db = getUsdbh().getWritableDatabase();
        pifia = "";
        if (db != null) {
            try {
                cont = db.update(tabla, valores, "ID=?", criterio);

            } catch (SQLException e) {
                pifia = e.getMessage();
                Util.miError("exeSQLUpd", pifia);
                Util.mostrarToastSimple(Util.getContext().getString(R.string.lb_cnx_error), true);
            } catch (Exception e) {
                pifia = e.getMessage();
                Util.miError("exeSQLUpd", pifia);
                Util.mostrarToastSimple(Util.getContext().getString(R.string.lb_cnx_error), true);
            } finally {
                try {
                    db.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    Util.miError("exeSqlUpd", e.getMessage());
                }
            }
        }
        return cont;
    }

    public static Cursor exeSQL(String strSQL) {
        Cursor rs = null;
        SQLiteDatabase db = getUsdbh().getWritableDatabase();
        pifia = "";
        if (db != null) {
            try {
                rs = db.rawQuery(strSQL, null);
            } catch (SQLException e) {
                pifia = e.getMessage();
                Util.miError("exeSQL", pifia);
                Util.mostrarToastSimple(Util.getContext().getString(R.string.lb_cnx_error), true);
            } catch (Exception e) {
                pifia = e.getMessage();
                Util.miError("exeSQL", pifia);
                Util.mostrarToastSimple(Util.getContext().getString(R.string.lb_cnx_error), true);
            }
        }
        return rs;
    }


    public static boolean exeUpdSQL(String strSQL) {
        SQLiteDatabase db = getUsdbh().getWritableDatabase();
        boolean ok = false;
        pifia = "";
        try {
            db.beginTransaction();
            db.execSQL(strSQL);
            db.setTransactionSuccessful();
            ok = true;
        } catch (SQLException e) {
            pifia = e.getMessage();
            Util.miError("exeUpdSQL", pifia);
            Util.mostrarToastSimple(Util.getContext().getString(R.string.lb_cnx_error), true);
        } catch (Exception e) {
            pifia = e.getMessage();
            Util.miError("exeUpdSQL", pifia);
            Util.mostrarToastSimple(Util.getContext().getString(R.string.lb_cnx_error), true);
        } finally {
            db.endTransaction();
        }
        return ok;
    }

    public static boolean SqlMasivo(String strSQL) {
        SQLiteDatabase db = getUsdbh().getWritableDatabase();
        boolean ok = false;
        pifia = "";
        try {
            db.beginTransaction();
            for (String tx : strSQL.split("\r\n")) {
                db.execSQL(tx);
            }
            db.setTransactionSuccessful();
            ok = true;
        } catch (SQLException e) {
            pifia = e.getMessage();
            Util.miError("SqlMasivo", pifia);
            Util.mostrarToastSimple(Util.getContext().getString(R.string.lb_cnx_error), true);
        } catch (Exception e) {
            pifia = e.getMessage();
            Util.miError("SqlMasivo", pifia);
            Util.mostrarToastSimple(Util.getContext().getString(R.string.lb_cnx_error), true);
        } finally {
            db.endTransaction();
        }
        return ok;
    }

    public static String Buscar(String strSQL, String strXDefecto) {
        Cursor rs = exeSQL(strSQL + " LIMIT 1");
        String valor = strXDefecto;

        try {
            if (rs != null) {
                if (rs.moveToNext())
                    if (rs.getString(0) != null) valor = rs.getString(0);
                rs.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Util.miError("Buscar", e.getMessage());
        }

        return valor;
    }

    public static void Desconecta() {
        if (getUsdbh() != null) getUsdbh().close();
    }

    public static void IniciarBD(Context ctx) {
        try {
            contexto = ctx;
            SQLiteDatabase db = getUsdbh().getWritableDatabase();
            db.close();
            getUsdbh().close();
        } catch (Exception e) {
            e.printStackTrace();
            Util.miError("IniciaDb", e.getMessage());
        }
    }

    private static ConectarBDMyWare getUsdbh() {
        if (usdbh == null)
            usdbh = new ConectarBDMyWare(contexto);
        return usdbh;
    }

    public static String getDatabaseName() {
        return getUsdbh().getDatabaseName();
    }
}