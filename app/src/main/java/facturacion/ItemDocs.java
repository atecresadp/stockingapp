package facturacion;

import com.atecresa.www.myware.Opciones;
import com.atecresa.www.myware.Util;

import static com.atecresa.www.myware.Opciones.TipoDoc.TRASPASO;

/**
 * Created by Gustavo on 26/01/2017.
 */
public class ItemDocs {
    private Opciones.TipoDoc tipo;
    private int id;
    private int numero;
    private String fecha;
    private String almorigen;
    private String almdestino;
    private String cliente;
    private String obs;
    private boolean tocado;

    public ItemDocs() {
        // Required empty public constructor
        super();
    }

    public ItemDocs(Opciones.TipoDoc pTipo, int pNumero, String pFecha, String pAlmOrigen, String pAlmDestino,
                    String pCliente, String pObs, int pTocado, int pId) {
        tipo = pTipo;
        numero = pNumero;
        fecha = pFecha;
        almorigen = (String) Util.noNulo(pAlmOrigen, "?");
        almdestino = (String) Util.noNulo(pAlmDestino, "?");
        cliente = (String) Util.noNulo(pCliente, "?");
        obs = (String) Util.noNulo(pObs, "");
        tocado = pTocado != 0;
        id = pId;
    }

    boolean getTocado() {
        return tocado;
    }

    String getAlmdestino() {
        return almdestino;
    }

    String getAlmorigen() {
        return almorigen;
    }

    String getCliente() {
        return cliente;
    }

    public String getFecha() {
        return fecha;
    }

    public int getNumero() {
        return numero;
    }

    public int getId() {
        return id;
    }

    public Opciones.TipoDoc getTipo() {
        return tipo;
    }

    String getObs() {
        return obs;
    }

    String getIncompleto() {
        if (almorigen.equals("?")) return "* El almacén de origen no está asignado";
        if (tipo.getResumenDoc() != Opciones.GrupoDocs.ALMACEN) {
            if (cliente.equals("?"))
                return "* El " + tipo.getSuCliente() + " no está asignado";
        } else if (tipo == TRASPASO && almdestino.equals("?"))
            return "* El almacén de destino no está asignado";

        return "";
    }
}
