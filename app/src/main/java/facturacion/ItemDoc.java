package facturacion;

public class ItemDoc {
    private String codigo;
    private String nombre;
    private double unidades;
    private String obs;
    private int idCaducidad;
    private int nLinea;
    private String sumaUnid;
    private String key;

    public ItemDoc() {
        // Required empty public constructor
        super();
    }

    public ItemDoc(String pCodigo, String pNombre, double pUnidades, String pObs, int pIdCaducidad, int pNLinea, String pSumaUnid, String pKey) {
        codigo = pCodigo;
        nombre = pNombre;
        unidades = pUnidades;
        obs = pObs;
        idCaducidad = pIdCaducidad;
        nLinea = pNLinea;
        sumaUnid = pSumaUnid;
        key = pKey;
    }

    public int getIdCaducidad() {
        return idCaducidad;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public String getObs() {
        return obs;
    }

    public double getUnidades() {
        return unidades;
    }

    public String getSumaUnid() {
        return sumaUnid;
    }

    public int getNLinea() {
        return nLinea;
    }

    public String getKey() {
        return key;
    }
}
