package facturacion;

import android.content.ContentValues;
import android.database.Cursor;

import com.atecresa.www.myware.Opciones;
import com.atecresa.www.myware.Util;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import SqLite.Cnx;

import static SqLite.Cnx.exeUpdSQL;

/**
 * Created by Gustavo on 17/12/2016.
 */

public class Maestro {

    private MyCustomObjectListener listener;

    private int id = -1;
    private int numero = -1;
    private Opciones.TipoDoc tipo;
    private int idCliente = -1;
    private Date fecha = Util.ahora();
    private int idAlmacen = Opciones.getIdAlmacen();
    ;
    private int idalmDest = -1;
    private int tocado = 1;
    private HashMap<String, Linea> lineas = new HashMap<String, Linea>();
    private String obs = "";

    interface MyCustomObjectListener {
        void UpdLinea(Linea l);

        void UpdCabecera();

        void DelLinea(int pos);
    }

    public Maestro(Opciones.TipoDoc pTipoDoc, int pNumero, MyCustomObjectListener listener) {
        this.listener = listener;
        tipo = pTipoDoc;
        numero = pNumero;
    }

    private void Guardar() {
        ContentValues campos = new ContentValues();
        boolean nuevo = false;
        if (getNumero() == -1) {
            nuevo = true;
            setNumero(Integer.parseInt(Cnx.Buscar("SELECT " + tipo.getNomCampo() + " FROM CONTADORES WHERE ID = 1;", "1")));
            Cnx.exeUpdSQL("UPDATE CONTADORES SET " + tipo.getNomCampo() + " = " + tipo.getNomCampo() + " + 1 WHERE ID = 1");
        }
        setTocado(1);
        campos.put("NUMERO", getNumero());
        campos.put("UUID", Opciones.getUuid());
        campos.put("TIPO", getTipo().getValor());
        campos.put("IDCLIPROV", getIdCliente());
        campos.put("FECHA", Util.sqliteFecha(getFecha()));
        campos.put("IDALM", getIdAlmacen());
        campos.put("IDALMDEST", getIdAlmDest());
        campos.put("OBS", getObs());
        campos.put("TOCADO", getTocado());

        if (nuevo)
            setId(Cnx.exeSQLInsert("MAESTRO", campos));
        else {
            int i = Cnx.exeSQLUpd("MAESTRO", campos, getId());
            if (i == 0)
                setId(Cnx.exeSQLInsert("MAESTRO", campos));
        }
        if (listener != null) listener.UpdCabecera();
    }

    int addMant(String tabla, ContentValues campos) {
        return Cnx.exeSQLInsert(tabla, campos);
    }

    int updMant(String tabla, int id, ContentValues campos) {
        return Cnx.exeSQLUpd(tabla, campos, id);
    }

    void AddLinea(int pIdArticulo, String pNombre, double pUnid, int pIdCaducidad, String pObs, int pNLinea) {
        Linea L = new Linea(this);
        L.setIdArticulo(pIdArticulo);
        L.setIdCaducidad(pIdCaducidad);
        L.setNomArt(pNombre);
        L.setObs(pObs);
        L.setNLinea(pNLinea);
        boolean modifica = (lineas.containsKey(L.getKey()));
        if (modifica) L = lineas.get(L.getKey());
        L.setUnidades(L.getUnidades() + pUnid);
        L.setSumaUnid(pUnid);

        lineas.put(L.getKey(), L);

        if (L.Cambiado) L.Guardar(modifica);

        if (listener != null) listener.UpdLinea(L);
    }

    void AddObsLin(ItemDoc it, String pObs) {
        if (lineas.containsKey(it.getKey())) {
            Linea L = lineas.get(it.getKey());
            if (!L.getObs().equals(pObs)) {
                L.setObs(pObs);
                L.Guardar(true);
                if (listener != null) listener.UpdLinea(L);
            }
        }
    }

    boolean BorraLinea(String key) {
        Linea L;
        boolean Ok = false;
        try {
            L = lineas.get(key);
            int numLin = L.getNLinea();
            int pos = lineas.size() - numLin - 1;
            if (L.Eliminar()) {
                Ok = true;
                lineas.remove(key);
                for (Map.Entry<String, Linea> entry : lineas.entrySet()) {
                    L = entry.getValue();
                    if (L.getNLinea() > numLin) {
                        L.setNLinea(L.getNLinea() - 1);
                    }
                }
                listener.DelLinea(pos);
            }
        } catch (java.lang.Exception e) {
            Ok = false;
        } finally {
            return Ok;
        }
    }

    public boolean eliminar() {
        exeUpdSQL("DELETE FROM DET_OBS WHERE EXISTS (SELECT 0 FROM DETALLE WHERE ID = DET_OBS.IDDETALLE AND IDMAESTRO = " + getId() + ")");
        exeUpdSQL("DELETE FROM DETALLE WHERE IDMAESTRO = " + getId());
        return (exeUpdSQL("DELETE FROM MAESTRO WHERE ID = " + getId()));
    }

    public boolean eliminarTodo() {
        exeUpdSQL("DELETE FROM DET_OBS WHERE EXISTS (SELECT 0 FROM DETALLE INNER JOIN MAESTRO ON MAESTRO.ID = DETALLE.IDMAESTRO WHERE MAESTRO.TOCADO = 0 AND DETALLE.ID = DET_OBS.IDDETALLE AND MAESTRO.UUID = '" + Opciones.getUuid() + "')");
        exeUpdSQL("DELETE FROM DETALLE WHERE EXISTS (SELECT 0 FROM MAESTRO WHERE MAESTRO.TOCADO = 0 AND ID = DETALLE.IDMAESTRO AND UUID = '" + Opciones.getUuid() + "')");
        return (exeUpdSQL("DELETE FROM MAESTRO WHERE TOCADO = 0 AND UUID = '" + Opciones.getUuid() + "'"));
    }

    private void leerCab() {

        Cursor rs = Cnx.exeSQL("SELECT MAESTRO.* FROM MAESTRO WHERE TIPO = " + getTipo().getValor() + " AND NUMERO = " + getNumero());
        if (rs.moveToFirst()) {
            this.id = rs.getInt(rs.getColumnIndex("ID"));
            this.numero = rs.getInt(rs.getColumnIndex("NUMERO"));
            this.tipo = Opciones.TipoDoc.getById(rs.getInt(rs.getColumnIndex("TIPO")));
            this.idCliente = rs.getInt(rs.getColumnIndex("IDCLIPROV"));
            this.fecha = Util.formatFecha(rs.getString(rs.getColumnIndex("FECHA")), true);
            this.idAlmacen = rs.getInt(rs.getColumnIndex("IDALM"));
            this.idalmDest = rs.getInt(rs.getColumnIndex("IDALMDEST"));
            this.obs = rs.getString(rs.getColumnIndex("OBS"));
            this.tocado = rs.getInt(rs.getColumnIndex("TOCADO"));
        }

        rs.close();

        if (listener != null) listener.UpdCabecera();
    }

    private void leerLin() {
        lineas.clear();
        int i = 0;
        Cursor rs = Cnx.exeSQL("SELECT DETALLE.*, (SELECT NOMBRE FROM ARTICULOS WHERE ID = DETALLE.IDARTICULO) AS ARTICULO, " +
                "(SELECT CODIGO FROM ARTICULOS WHERE ID = DETALLE.IDARTICULO) AS CODART, IDCADUCIDAD, " +
                "(SELECT OBS FROM DET_OBS WHERE IDDETALLE = DETALLE.ID) AS OBS " +
                "FROM DETALLE WHERE IDMAESTRO = " + getId() + " ORDER BY ID");

        if (rs.moveToFirst()) {
            do {
                Linea L = new Linea(this);
                L.setId(rs.getInt(rs.getColumnIndex("ID")));
                L.setIdArticulo(rs.getInt(rs.getColumnIndex("IDARTICULO")));
                L.setFecha(Util.formatFecha(rs.getString(rs.getColumnIndex("FECHA")), true));
                L.setIdCaducidad(rs.getInt(rs.getColumnIndex("IDCADUCIDAD")));
                L.setNomArt(Util.noNulo(rs.getString(rs.getColumnIndex("ARTICULO")), "").toString());
                L.setCodArt(Util.noNulo(rs.getString(rs.getColumnIndex("CODART")), "").toString());
                L.setObs(Util.noNulo(rs.getString(rs.getColumnIndex("OBS")), "").toString());
                L.setNLinea(i++);
                L.setUnidades(rs.getDouble(rs.getColumnIndex("UNID")));
                L.setSumaUnid(L.getUnidades());
                L.Cambiado = false;
                lineas.put(L.getKey(), L);
                if (listener != null) listener.UpdLinea(L);
            } while (rs.moveToNext());
        }
        rs.close();
    }

    public void leerDoc(boolean soloCab) {
        if (getNumero() == -1)
            listener.UpdCabecera();
        else {
            leerCab();
            if (!soloCab) leerLin();
        }
    }

    public int getId() {
        if (this.id == -1)
            id = Integer.parseInt(Cnx.Buscar("SELECT ID FROM MAESTRO WHERE NUMERO = " + numero + " AND TIPO = " + tipo.getValor(), "-1"));
        return this.id;
    }

    public void setId(int pId) {
        this.id = pId;
    }

    public int getIdCliente() {
        return idCliente;
    }

    void setIdCliente(int cliente) {
        if (cliente != idCliente) {
            idCliente = cliente;
            Guardar();
        }
    }

    public String getNomCliente() {
        return Cnx.Buscar("SELECT NOMBRE FROM CLIPROV WHERE ID = '" + idCliente + "'", "");
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getIdAlmacen() {
        return idAlmacen;
    }

    void setIdAlmacen(int idAlmacen) {
        if (idAlmacen != this.idAlmacen) {
            setTocado(1);
            this.idAlmacen = idAlmacen;
            Guardar();
        }
    }

    public int getIdAlmDest() {
        return idalmDest;
    }

    void setIdalmDest(int idalmDest) {
        if (idalmDest != this.idalmDest) {
            this.idalmDest = idalmDest;
            Guardar();
        }
    }

    public String getNomAlmOrigen() {
        return Cnx.Buscar("SELECT NOMBRE FROM ALMACENES WHERE ID = " + idAlmacen, "");
    }

    public String getNomAlmDestino() {
        return Cnx.Buscar("SELECT NOMBRE FROM ALMACENES WHERE ID = " + idalmDest, "");
    }

    public int getNumArticulos() {
        if (lineas.size() == 0)
            return Integer.parseInt(Cnx.Buscar("SELECT COUNT(0) FROM DETALLE WHERE IDMAESTRO = " + getId(), "0"));
        return lineas.size();
    }

    public double getTotalUnidades() {
        Linea L;
        if (lineas.size() == 0)
            return Double.parseDouble(Cnx.Buscar("SELECT SUM(UNID) FROM DETALLE WHERE IDMAESTRO = " + getId(), "0"));
        double total = 0;
        for (Map.Entry<String, Linea> entry : lineas.entrySet()) {
            L = entry.getValue();
            total = total + L.getUnidades();
        }
        return total;
    }

    public int getNumero() {
        return numero;
    }

    private void setNumero(int numero) {
        this.numero = numero;
    }

    public String getObs() {
        return obs;
    }

    void setObs(String obs) {
        if (!obs.equals(this.obs)) {
            this.obs = obs;
            Guardar();
        }
    }

    public int getTocado() {
        return tocado;
    }

    private void setTocado(int tocado) {
        this.tocado = tocado;
    }

    public Opciones.TipoDoc getTipo() {
        return tipo;
    }

    private void setTipo(Opciones.TipoDoc tipo) {
        this.tipo = tipo;
    }

    public int getNumLineas() {
        return lineas.size();
    }

    class Linea {
        private Maestro miMaestro;

        private int id = -1;
        private int idArticulo = -1;
        private String CodArt = "";
        private double Unidades = 0;
        private String SumaUnid = "";
        private String NomArt = "";
        private int IdCaducidad;
        private String Obs = "";
        private int NLinea = 0;
        private Date fecha = Util.ahora();

        boolean Cambiado;

        Linea(Maestro miPapi) {
            miMaestro = miPapi;
        }

        String getKey() {
            return String.valueOf(idArticulo) + "|" + String.valueOf(IdCaducidad);
        }

        public int getIdArticulo() {
            return idArticulo;
        }

        void setIdArticulo(int idart) {
            if (!Cambiado) Cambiado = idArticulo != idart;
            idArticulo = idart;
        }

        String getNomArt() {
            if (NomArt.equals(""))
                NomArt = Cnx.Buscar("SELECT NOMBRE FROM ARTICULOS WHERE ID = '" + idArticulo + "'", "Artículo Nuevo " + CodArt);

            return NomArt;
        }

        void setNomArt(String nomArt) {
            NomArt = nomArt;
        }

        String getCodArt() {
            if (CodArt.equals(""))
                CodArt = Cnx.Buscar("SELECT CODIGO FROM ARTICULOS WHERE ID = '" + idArticulo + "'", "");
            return CodArt;
        }

        void setCodArt(String codArt) {
            CodArt = codArt;
        }

        String getObs() {
            return Obs;
        }

        void setObs(String obs) {
            if (!Cambiado) Cambiado = !Obs.equals(obs);
            Obs = obs;
        }

        double getUnidades() {
            return Unidades;
        }


        void setFecha(Date fecha) {
            this.fecha = fecha;
        }

        Date getFecha() {
            return fecha;
        }

        void setUnidades(double unidades) {
            if (!Cambiado) Cambiado = Unidades != unidades;
            Unidades = unidades;
        }

        int getNLinea() {
            return NLinea;
        }

        void setNLinea(int NLinea) {
            this.NLinea = NLinea;
        }

        int getIdCaducidad() {
            return IdCaducidad;
        }

        void setIdCaducidad(int idCaducidad) {
            if (!Cambiado) Cambiado = IdCaducidad != idCaducidad;
            IdCaducidad = idCaducidad;
        }

        String getSumaUnid() {
            return SumaUnid;
        }

        void setSumaUnid(double sumaUnid) {
            SumaUnid = SumaUnid + Util.formatearUnidades(sumaUnid, true);
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getId() {
            if (id == -1)
                id = Integer.parseInt(Cnx.Buscar("SELECT ID FROM DETALLE WHERE IDMAESTRO = " + miMaestro.getId() + " AND IDARTICULO = '" + idArticulo + "' AND IDCADUCIDAD = " + IdCaducidad, "-1"));
            return id;
        }

        void Guardar(boolean upd) {
            if (miMaestro.getNumero() == -1 || miMaestro.getTocado() == 0) miMaestro.Guardar();
            if (upd) {
                Cnx.exeUpdSQL("UPDATE DETALLE SET UNID = " + Util.sqliteNumero(Unidades) + ", " +
                        "IDCADUCIDAD = " + IdCaducidad + " WHERE ID = " + id);
                if (Obs.length() > 1)
                    Cnx.exeUpdSQL("INSERT OR REPLACE INTO DET_OBS (IDDETALLE, OBS) VALUES (" + id + ", '" + Obs + "')");
            } else {
                ContentValues valores = new ContentValues();
                valores.put("IDMAESTRO", miMaestro.getId());
                valores.put("IDARTICULO", idArticulo);
                valores.put("UNID", Util.sqliteNumero(Unidades));
                valores.put("IDCADUCIDAD", IdCaducidad);
                id = Cnx.exeSQLInsert("DETALLE", valores);
            }
        }

        boolean Eliminar() {
            boolean r;
            exeUpdSQL("DELETE FROM DET_OBS WHERE IDDETALLE = " + id);
            r = Cnx.exeUpdSQL("DELETE FROM DETALLE WHERE ID = " + id);
            id = -1;
            return r;
        }
    }
}

