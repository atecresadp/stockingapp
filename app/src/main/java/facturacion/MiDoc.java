package facturacion;

import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;

import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.atecresa.www.myware.MiBrowser;
import com.atecresa.www.myware.Opciones;
import com.atecresa.www.myware.R;
import com.atecresa.www.myware.Util;

import java.util.ArrayList;
import java.util.Timer;

import SqLite.Cnx;

import static com.atecresa.www.myware.Opciones.TipoDoc.TRASPASO;
import static facturacion.MiDoc.MiArticulo.art_codigo;
import static facturacion.MiDoc.MiArticulo.art_idArticulo;
import static facturacion.MiDoc.MiArticulo.art_idCaducidad;
import static facturacion.MiDoc.MiArticulo.art_nombre;
import static facturacion.MiDoc.MiArticulo.art_tipo;

public class MiDoc extends AppCompatActivity {

    final int BUSCA_ART = 1, BUSCA_ALMO = 2, BUSCA_ALMD = 3, BUSCA_CLI = 4;

    AutoCompleteTextView codArt = null;
    EditText unids = null;
    ImageButton img = null;
    RecyclerView recVLista;
    AdapterDoc adapter = null;
    RecyclerView.LayoutManager layoutMg;

    Maestro Doc;

    SearchView searchView;
    ArrayList<ItemDoc> items;

    private int lectAuto = 1;
    private Timer parada;
    private Toolbar toolbar;

    static class MiArticulo {
        static int art_idCaducidad = -1; //TODO: Desarrollar tema de caducidades
        static int art_idArticulo = -1;
        static String art_tipo = "";
        static String art_codigo = "";
        static String art_nombre = "";
    }

    String obs = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mi_doc);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recVLista = (RecyclerView) findViewById(R.id.ListaItems);
        recVLista.setHasFixedSize(true);

        layoutMg = new LinearLayoutManager(this);
        recVLista.setLayoutManager(layoutMg);

        adapter = new AdapterDoc(items);
        recVLista.setAdapter(adapter);

        codArt = (AutoCompleteTextView) findViewById(R.id.CodArt);
        unids = (EditText) findViewById(R.id.Unids);
        img = (ImageButton) findViewById(R.id.img_cam);
        items = new ArrayList<ItemDoc>();

        codArt.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Opciones.getTamCod() + Opciones.getSufijo().length())});
        unids.setFilters(new InputFilter[]{new InputFilter.LengthFilter(8)});
        codArt.setRawInputType(InputType.TYPE_CLASS_TEXT);
        codArt.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        unids.setImeOptions(EditorInfo.IME_ACTION_GO);

        if (Opciones.getLecturaAuto()) {
            unids.setText(String.valueOf(lectAuto));
            unids.setEnabled(false);
        }

        final RecyclerView rv = (RecyclerView) findViewById(R.id.ListaItems);
        adapter = new AdapterDoc(items);
        rv.setAdapter(adapter);


        Opciones.TipoDoc idDoc = (Opciones.TipoDoc) getIntent().getExtras().get("IdDoc");
        int num = getIntent().getExtras().getInt("Numero");

        Doc = new Maestro(idDoc, num, new Maestro.MyCustomObjectListener() {

            @Override
            public void UpdLinea(Maestro.Linea l) {
                ItemDoc it = new ItemDoc(l.getCodArt(), l.getNomArt(), l.getUnidades(), l.getObs(),
                        l.getIdCaducidad(), l.getNLinea(), l.getSumaUnid(), l.getKey());
                int pos = items.size() - l.getNLinea() - 1;
                if (l.getNLinea() >= items.size()) {
                    items.add(0, it);
                } else {
                    items.set(pos, it);
                    rv.scrollToPosition(pos);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void UpdCabecera() {
                MostrarCabecera();
            }

            @Override
            public void DelLinea(int pos) {

            }
        });
        Doc.leerDoc(false);

        if (!Opciones.getCamaraOn()) {
            codArt.setEms(8);
            unids.setEms(5);
            img.setVisibility(ImageButton.GONE);
        }

        img.setOnClickListener(new ImageButton.OnClickListener() {
            @Override
            public void onClick(View v) {
                escanear();
            }
        });

        rv.addOnItemTouchListener(
                new RecyclerItemClickListener(this, rv, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        codArt.setText(items.get(position).getCodigo());
                        tratarCodArt();
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        altaObserva(items.get(position)).show();
                    }
                })
        );

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                onItemRemove(viewHolder, rv);
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(rv);

        codArt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    Util.enfoca(unids, MiDoc.this);
                    return false;
                } else
                    return true;
            }
        });

        codArt.setOnKeyListener(new TextView.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_UP) {
                    Util.enfoca(unids, MiDoc.this);
                }
                return false;
            }
        });


        codArt.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                if (!Opciones.getSufijo().equals("")) {
                    if (codArt.getText().toString().contains(Opciones.getSufijo())) {
                        codArt.setText(codArt.getText().toString().replace(Opciones.getSufijo(), ""));
                        tratarCodArt();
                    }
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        unids.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    tratarCodArt();
            }
        });

        unids.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_GO) || ((event.getKeyCode() == KeyEvent.KEYCODE_ENTER) &&
                        (event.getAction() == KeyEvent.ACTION_DOWN))) {
                    tratarUnid();
                    return true;
                } else return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_midoc, menu);

        MenuItem searchItem = menu.findItem(R.id.menu_buscador);

        MenuItem it;
        it = menu.findItem(R.id.id_clienteDoc);
        it.setVisible(Doc.getTipo().getResumenDoc() == Opciones.GrupoDocs.VENTAS);

        it = menu.findItem(R.id.id_proveedorDoc);
        it.setVisible(Doc.getTipo().getResumenDoc() == Opciones.GrupoDocs.COMPRAS);

        it = menu.findItem(R.id.id_almacenDestino);
        it.setVisible(Doc.getTipo() == TRASPASO);

        it = menu.findItem(R.id.id_lecturaAuto);
        it.setVisible(Opciones.getLecturaAuto());

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            String uri = intent.getDataString();
            uri = uri.substring(uri.lastIndexOf('/') + 1);
            codArt.setText(uri);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (resultCode != RESULT_OK) return;
        if (requestCode == 0) {       //TIPO DE PETICIÓN ANTES REGISTRADA
            String codigo = intent.getStringExtra("SCAN_RESULT"); //TEXTO RECIBIDO
            codArt.setText(codigo); //AQUÍ LO ASIGNO A UNA CAJA DE TEXTO, SÓLO DE EJEMPLO
            Util.enfoca(codArt, MiDoc.this);
        } else if (requestCode == this.BUSCA_ART) {
            codArt.setText(Cnx.Buscar("SELECT CODIGO FROM ARTICULOS WHERE ID = " + intent.getStringExtra("itemSel"), ""));
            Util.enfoca(codArt, MiDoc.this);
        } else if (requestCode == this.BUSCA_ALMO) {
            Doc.setIdAlmacen(Integer.parseInt(intent.getStringExtra("itemSel")));
            MostrarCabecera();
        } else if (requestCode == this.BUSCA_ALMD) {
            Doc.setIdalmDest(Integer.parseInt(intent.getStringExtra("itemSel")));
            MostrarCabecera();
        } else if (requestCode == this.BUSCA_CLI) {
            Doc.setIdCliente(Integer.parseInt(intent.getStringExtra("itemSel")));
            MostrarCabecera();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.id_info) {
            Util.verDetalle(Doc, this);
        } else if (item.getItemId() == R.id.id_lecturaAuto) {
            if (Opciones.getLecturaAuto()) {
                if (lectAuto == 1) {
                    lectAuto = -1;
                    item.setIcon(R.drawable.ic_menosuno);
                    item.setTitle("Sumar unidades");
                } else {
                    item.setIcon(R.drawable.ic_masuno);
                    item.setTitle("Restar unidades");
                    lectAuto = 1;
                }
                unids.setText(String.valueOf(lectAuto));
            }
        } else if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        } else if (item.getItemId() == R.id.id_observacion) {
            altaObserva(null).show();
        } else if (item.getItemId() == R.id.id_observacionLin) {
            Util.msgBox("Observación línea", "Pulse de forma prolongada sobre un artículo de la lista", this);
        } else if (item.getItemId() == R.id.id_almacenOrigen) {
            Intent inte = new Intent(this, MiBrowser.class);
            inte.putExtra("SQLbusca", "SELECT ID, CODIGO, NOMBRE FROM ALMACENES");
            inte.putExtra("TituloBrw", "Almacén de Origen");
            startActivityForResult(inte, this.BUSCA_ALMO);
        } else if (item.getItemId() == R.id.menu_buscador) {
            Intent inte = new Intent(this, MiBrowser.class);
            inte.putExtra("SQLbusca", "SELECT ARTICULOS.ID, ARTICULOS.CODIGO || ' ' || RELACIONES.CODEXTERNO, ARTICULOS.NOMBRE FROM ARTICULOS LEFT JOIN RELACIONES ON ARTICULOS.ID = RELACIONES.IDARTICULO");
            inte.putExtra("TituloBrw", "Búsqueda de Artículos");
            startActivityForResult(inte, this.BUSCA_ART);
        } else if (item.getItemId() == R.id.id_almacenDestino) {
            Intent inte = new Intent(this, MiBrowser.class);
            inte.putExtra("SQLbusca", "SELECT ID, CODIGO, NOMBRE FROM ALMACENES");
            inte.putExtra("TituloBrw", "Almacén de Destino");
            startActivityForResult(inte, this.BUSCA_ALMD);
        } else if (item.getItemId() == R.id.id_clienteDoc) {
            Intent inte = new Intent(this, MiBrowser.class);
            inte.putExtra("SQLbusca", "SELECT ID, CODIGO, NOMBRE FROM CLIPROV");
            inte.putExtra("TituloBrw", "Búsqueda de Cliente");
            inte.putExtra("Criterio", "TIPO = 'C'");
            startActivityForResult(inte, this.BUSCA_CLI);
        } else if (item.getItemId() == R.id.id_proveedorDoc) {
            Intent inte = new Intent(this, MiBrowser.class);
            inte.putExtra("SQLbusca", "SELECT ID, CODIGO, NOMBRE FROM CLIPROV");
            inte.putExtra("TituloBrw", "Búsqueda de Proveedor");
            inte.putExtra("Criterio", "TIPO = 'P'");
            startActivityForResult(inte, this.BUSCA_CLI);
        }
        return super.onOptionsItemSelected(item);
    }

    private void MostrarCabecera() {
        String subTit = "";
        getSupportActionBar().setTitle(Doc.getTipo().getNombre());
        switch (Doc.getTipo()) {
            case REG_INVENTA:
                subTit = "Almacén: " + Doc.getNomAlmOrigen();
                break;
            case TRASPASO:
                subTit = "De " + Doc.getNomAlmOrigen() + " a " + Doc.getNomAlmDestino();
                break;
            default:
                subTit = Doc.getNomCliente();
        }

        getSupportActionBar().setSubtitle(subTit);
        if (Doc.getTocado() == 0)
            toolbar.setBackgroundResource(R.color.colorPrimary);
        else
            toolbar.setBackgroundResource(R.color.colorAccent);

    }

    private void escanear() {
        try {
            //INTENT QUE CONECTA CON LA APP BARCODE SCANNER
            Intent intent = new Intent("com.google.zxing.client.android.SCAN");
            intent.putExtra("SCAN_MODE", "PRODUCT_MODE");
            //intent.putExtra("SCAN_MODE", "QR_CODE_MODE");  //OPCIONAL PARA ESCANEAR EN MODO QR
            startActivityForResult(intent, 0);        //EL 0 IDENTIFICARÁ AL TIPO DE PETICIÓN
        } catch (ActivityNotFoundException anfe) {
            //SI NO TENEMOS INSTALADO EL BARCODE SCANNER, MOSTRAMOS UN MENSAJE PARA QUE EL USUARIO LO INSTALE DESDE GOOGLE PLAY
            final AlertDialog d = new AlertDialog.Builder(MiDoc.this) //SUSTITUIR EL PARÁMETRO POR EL NOMBRE DE NUESTRA ACTIVITY
                    .setPositiveButton("Si",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Uri uri = Uri.parse("https://market.android.com/details?id=com.google.zxing.client.android&hl=es");
                                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                    startActivity(intent);
                                }
                            }).setNegativeButton("Cancelar", null)
                    .setMessage("No se encuentra el software Barcode Scanner. ¿Desea instalarlo?")
                    .create();

            d.show();
        }
    }

    private boolean validarCodigo(String cod) {
        MiArticulo.art_idArticulo = -1;
        MiArticulo.art_tipo = "";
        art_idCaducidad = -1;
        MiArticulo.art_codigo = "";
        art_nombre = "";
        Cursor rs = Cnx.exeSQL("SELECT ARTICULOS.ID, ARTICULOS.TIPO, ARTICULOS.CODIGO, ARTICULOS.NOMBRE " +
                "FROM ARTICULOS LEFT JOIN RELACIONES ON RELACIONES.IDARTICULO = ARTICULOS.ID" +
                " WHERE ARTICULOS.CODIGO = '" + cod + "' OR RELACIONES.CODEXTERNO = '" + cod + "'");
        if (rs.moveToFirst()) {
            MiArticulo.art_idArticulo = rs.getInt(rs.getColumnIndex("ID"));
            MiArticulo.art_tipo = rs.getString(rs.getColumnIndex("TIPO"));
            MiArticulo.art_codigo = rs.getString(rs.getColumnIndex("CODIGO"));
            art_nombre = rs.getString(rs.getColumnIndex("NOMBRE"));
            rs.close();
            return !art_tipo.equals("D");
        }

        return false;
    }

    private void tratarCodArt() {
        new LeerCod(codArt.getText().toString()).execute();
    }

    private void tratarUnid() {
        double uds = 0;
        unids.setError(null);

        try {
            uds = Double.parseDouble(unids.getText().toString());
            if (codArt.getError() == null && unids.getError() == null) {
                Doc.AddLinea(art_idArticulo, "", uds, art_idCaducidad, obs, items.size());
                Util.enfoca(codArt, MiDoc.this);
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
            unids.setError("No es un número válido.");
        }


    }

    public void onItemRemove(final RecyclerView.ViewHolder viewHolder, final RecyclerView recyclerView) {
        final int adapterPosition = viewHolder.getAdapterPosition();
        final ItemDoc it = items.get(adapterPosition);

        Snackbar snackbar = Snackbar
                .make(recyclerView, "Registro eliminado", Snackbar.LENGTH_LONG)
                .setAction("Deshacer", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        items.add(adapterPosition, it);
                        adapter.notifyItemInserted(adapterPosition);
                        recyclerView.scrollToPosition(adapterPosition);
                    }
                });
        snackbar.setCallback(new Snackbar.Callback() {
            @Override
            public void onDismissed(Snackbar snackbar, int event) {
                if (event == Snackbar.Callback.DISMISS_EVENT_TIMEOUT) {
                    Doc.BorraLinea(it.getKey());
                }
            }
        });
        snackbar.show();
        items.remove(adapterPosition);
        adapter.notifyItemRemoved(adapterPosition);
    }

    private AlertDialog altaObserva(final ItemDoc lin) {
        String nome = "";
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Escriba una observación");

        final EditText input = new EditText(this);
        if (lin == null) {
            nome = Doc.getTipo().getNombre() + " " + Doc.getNumero();
            input.setText(Doc.getObs());
        } else {
            nome = "Artículo: " + lin.getNombre();
            input.setText(lin.getObs());
        }

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        builder.setView(input);
        builder.setIcon(R.mipmap.ic_launcher);

        builder.setPositiveButton("Aceptar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (lin == null)
                            Doc.setObs(input.getText().toString());
                        else
                            Doc.AddObsLin(lin, input.getText().toString());
                    }
                });

        builder.setNegativeButton("Cancelar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        builder.setMessage(nome);
        return builder.create();
    }

    private AlertDialog trataDescatalogado() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        LayoutInflater inflater = this.getLayoutInflater();

        builder.setTitle("Artículo DESCATALOGADO");

        builder.setMessage(Cnx.Buscar("SELECT CODIGO || ' ' || NOMBRE FROM ARTICULOS WHERE ID = " + art_idArticulo, "") + "\n" +
                "Este artículo figura como descatalogado, ¿quiere ponerlo activo?");

        builder.setPositiveButton("Sí",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ContentValues val = new ContentValues();
                        MiArticulo.art_tipo = "X";
                        val.put("TIPO", "X");
                        ;
                        val.put("TOCADO", 1);
                        Doc.updMant("ARTICULOS", art_idArticulo, val);
                    }
                });

        builder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        return builder.create();
    }

    private AlertDialog altaArticulo() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        LayoutInflater inflater = this.getLayoutInflater();

        builder.setTitle("Crear nuevo artículo");

        View v = inflater.inflate(R.layout.alta_articulo, null);

        builder.setView(v);

        final TextView aCodigo = (TextView) v.findViewById(R.id.alta_Codigo);
        final TextView aNombre = (TextView) v.findViewById(R.id.alta_NomArt);
        final Spinner aIdFam = (Spinner) v.findViewById(R.id.alta_Familia);
        final TextView aCosto = (TextView) v.findViewById(R.id.alta_Costo);
        final TextView aPvp = (TextView) v.findViewById(R.id.alta_Pvp);
        aCodigo.setText(Util.LlenaCeros(codArt.getText().toString()));
        aCodigo.setEnabled(false);
        aIdFam.setPrompt("Familia del artículo");

        Cursor rs = Cnx.exeSQL("SELECT ID, NOMBRE FROM FAMILIAS");
        StringBuilder nombres = new StringBuilder();

        if (rs.moveToFirst()) {
            do {
                nombres.append(rs.getString(rs.getColumnIndex("NOMBRE")) + "~~");
            } while (rs.moveToNext());
        }
        if (nombres.length() > 0) {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, nombres.toString().split("~~"));
            aIdFam.setAdapter(adapter);
        }

        builder.setPositiveButton("Guardar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (aNombre.getText().toString().equals(""))
                            aNombre.setText("Artículo " + aCodigo.getText().toString());
                        if (aCosto.getText().toString().equals("")) aCosto.setText("0");
                        if (aPvp.getText().toString().equals("")) aPvp.setText("0");
                        ContentValues val = new ContentValues();
                        //val.put("ID", Cnx.Buscar("SELECT MAX(ID) + 1 FROM ARTICULO", "1"));
                        val.put("CODIGO", aCodigo.getText().toString());
                        val.put("NOMBRE", aNombre.getText().toString());
                        val.put("IDFAMILIA", Cnx.Buscar("SELECT ID FROM FAMILIAS WHERE NOMBRE = '" +
                                aIdFam.getSelectedItem().toString() + "'", "1"));
                        val.put("COSTO", Double.parseDouble(aCosto.getText().toString()));
                        val.put("PVP", Double.parseDouble(aPvp.getText().toString()));
                        val.put("TOCADO", 1);
                        Doc.addMant("ARTICULOS", val);
                        Util.enfoca(codArt, MiDoc.this);
                    }
                }
        );

        builder.setNegativeButton("Cancelar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Util.enfoca(codArt, MiDoc.this);
                        dialog.cancel();
                    }
                }
        );

        return builder.create();
    }

    public static class RecyclerItemClickListener implements RecyclerView.OnItemTouchListener {
        private OnItemClickListener mListener;

        public interface OnItemClickListener {
            public void onItemClick(View view, int position);

            public void onLongItemClick(View view, int position);
        }

        GestureDetector mGestureDetector;

        public RecyclerItemClickListener(Context context, final RecyclerView recyclerView, OnItemClickListener listener) {
            mListener = listener;
            mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && mListener != null) {
                        mListener.onLongItemClick(child, recyclerView.getChildAdapterPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
            View childView = view.findChildViewUnder(e.getX(), e.getY());
            if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
                mListener.onItemClick(childView, view.getChildAdapterPosition(childView));
                return true;
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView view, MotionEvent motionEvent) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
        }
    }


    public class LeerCod extends AsyncTask<Void, String, Boolean> {
        private String codigo = "";

        LeerCod(String pCodigo) {
            codArt.setError(null);
            codigo = Util.LlenaCeros(pCodigo);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            return validarCodigo(codigo);
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            art_codigo = codigo;
            if (!success) {
                if (art_tipo.equals("D")) {
                    trataDescatalogado().show();
                    codArt.setError("Artículo descatalogado");
                } else if (Opciones.getAvisoArtInex() && !Opciones.getCrearArt()) {
                    codArt.setError("Este código de artículo no está registrado");
                } else if (Opciones.getCrearArt()) {
                    altaArticulo().show();
                } else if (!Opciones.getCrearArt()) {
                    ContentValues val = new ContentValues();
                    art_nombre = "Artículo nuevo " + codigo;
                    art_tipo = "X";
                    val.put("CODIGO", art_codigo);
                    val.put("NOMBRE", art_nombre);
                    val.put("TOCADO", 1);
                    art_idArticulo = Doc.addMant("ARTICULOS", val);
                } else {
                    altaArticulo().show();
                }
            }
            codArt.setText(art_codigo);
            if (codArt.getError() == null) {
                if (Opciones.getLecturaAuto())
                    tratarUnid();
                Snackbar.make(codArt, art_nombre, Snackbar.LENGTH_SHORT).show();
            }
        }
    }
}
