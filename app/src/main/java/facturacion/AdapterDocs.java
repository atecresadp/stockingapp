package facturacion;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.atecresa.www.myware.LoginActivity;
import com.atecresa.www.myware.MainActivity;
import com.atecresa.www.myware.Nube;
import com.atecresa.www.myware.Opciones;
import com.atecresa.www.myware.R;
import com.atecresa.www.myware.Util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.util.ArrayList;

import static com.atecresa.www.myware.Opciones.GrupoDocs.ALMACEN;
import static com.atecresa.www.myware.Opciones.TipoDoc.TRASPASO;

/**
 * Created by Gustavo on 26/01/2017.
 */
public class AdapterDocs extends RecyclerView.Adapter<AdapterDocs.ViewHolder> {

    private ArrayList<ItemDocs> items;
    private MainActivity miPapi = null;

    public AdapterDocs(ArrayList<ItemDocs> items, MainActivity owner) {
        this.items = items;
        miPapi = owner;
    }

    public AdapterDocs(ArrayList<ItemDocs> items) {
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_main_docs, parent, false);
        // set the view's size, margins, paddings and layout parameters

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ItemDocs it = items.get(position);
        try {
            holder.mFecha.setText(Util.formatearFecha(it.getFecha()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (it.getTipo() == TRASPASO)
            holder.mAlmOrig.setText(String.format(Util.getContext().getString(R.string.lb_origen), it.getAlmorigen()));
        else
            holder.mAlmOrig.setText(Util.getContext().getString(R.string.lb_almacen) + it.getAlmorigen());
        if (it.getAlmorigen().equals(""))
            holder.mAlmOrig.setVisibility(View.GONE);
        else
            holder.mAlmOrig.setVisibility(View.VISIBLE);
        holder.mAlmDest.setText(Util.getContext().getString(R.string.lb_destino) + it.getAlmdestino());
        if (it.getAlmdestino().equals("") || it.getTipo() != TRASPASO)
            holder.mAlmDest.setVisibility(View.GONE);
        else
            holder.mAlmDest.setVisibility(View.VISIBLE);
        holder.mCli.setText(it.getTipo().getSuCliente() + ": " + it.getCliente());
        if (it.getCliente().equals("") || it.getTipo().getResumenDoc() == ALMACEN)
            holder.mCli.setVisibility(View.GONE);
        else
            holder.mCli.setVisibility(View.VISIBLE);
        holder.mObs.setText(Util.getContext().getString(R.string.lb_observacion) + it.getObs());
        if (it.getObs().equals(""))
            holder.mObs.setVisibility(View.GONE);
        else
            holder.mObs.setVisibility(View.VISIBLE);
        holder.mIco.setImageResource(it.getTipo().getIcono());
        holder.mNombre.setText(it.getTipo().getNombre() + " " + it.getNumero());
        holder.mAviso.setText(it.getIncompleto());
        if (it.getIncompleto().equals(""))
            holder.mAviso.setVisibility(View.GONE);
        else {
            holder.mAviso.setBackgroundResource(R.color.colorAccent);
            holder.mAviso.setVisibility(View.VISIBLE);
        }
        if (it.getTocado())
            holder.mTocado.setImageResource(R.drawable.ic_comunicar);
        else
            holder.mTocado.setImageResource(R.drawable.ic_nube);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        TextView mFecha, mAlmOrig, mAlmDest, mCli, mObs, mNombre, mAviso;
        ImageView mIco, mTocado, mMenu;

        ViewHolder(View v) {
            super(v);
            mIco = (ImageView) v.findViewById(R.id.itemM_imgDoc);
            mNombre = (TextView) v.findViewById(R.id.itemM_NomDoc);
            mCli = (TextView) v.findViewById(R.id.itemM_clienteDoc);
            mAlmDest = (TextView) v.findViewById(R.id.itemM_AlmDestDoc);
            mAlmOrig = (TextView) v.findViewById(R.id.itemM_AlmOrigDoc);
            mFecha = (TextView) v.findViewById(R.id.itemM_fechaDoc);
            mObs = (TextView) v.findViewById(R.id.itemM_obs);
            mAviso = (TextView) v.findViewById(R.id.itemM_aviso);
            mTocado = (ImageView) v.findViewById(R.id.itemM_nube);
            mMenu = (ImageView) v.findViewById(R.id.itemM_Menu);
            LinearLayout b = (LinearLayout) v.findViewById(R.id.itemM_layout);
            mMenu.setOnClickListener(this);
            b.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == mMenu.getId()) {
                menuContext(v, getAdapterPosition());
            } else {
                abrirDoc(v, items.get(getAdapterPosition()));
            }
        }

        @Override
        public boolean onLongClick(View v) {
            return false;
        }
    }

    public void clear() {
        int size = this.items.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.items.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }

    private void menuContext(final View view, final int position) {
        final ItemDocs it = items.get(position);
        ImageView miMenu = (ImageView) view.findViewById(R.id.itemM_Menu);
        PopupMenu popupMenu = new PopupMenu(view.getContext(), miMenu);

        try {
            Field[] fields = popupMenu.getClass().getDeclaredFields();
            for (Field field : fields) {
                if ("mPopup".equals(field.getName())) {
                    field.setAccessible(true);
                    Object menuPopupHelper = field.get(popupMenu);
                    Class<?> classPopupHelper = Class.forName(menuPopupHelper
                            .getClass().getName());
                    Method setForceIcons = classPopupHelper.getMethod(
                            "setForceShowIcon", boolean.class);
                    setForceIcons.invoke(menuPopupHelper, true);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        popupMenu.getMenuInflater().inflate(R.menu.menu_item_doc, popupMenu.getMenu());
        popupMenu.getMenu().findItem(R.id.itdocs_Enviar).setEnabled(it.getTocado());
        popupMenu.show();
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.itdocs_Editar:
                        abrirDoc(view, items.get(position));
                        break;
                    case R.id.itdocs_Enviar:
                    case R.id.itdocs_pdf:
                        SubirDoc com = new SubirDoc(view, item.getItemId() == R.id.itdocs_pdf, it);
                        com.execute();
                        break;
                    case R.id.itdocs_detalle:
                        Util.verDetalle(new Maestro(it.getTipo(), it.getNumero(), null), view.getContext());
                        break;
                    case R.id.itdocs_Eliminar:
                        Util.eliminaDoc(view, items, position, AdapterDocs.this, false).show();
                        mostrarResumen();
                        break;
                    case R.id.itAllDocs_Eliminar:
                        Util.eliminaDoc(view, items, position, AdapterDocs.this, true).show();
                        mostrarResumen();
                        break;
                }
                return true;
            }
        });
    }

    private void mostrarResumen() {
        if (miPapi != null) {
            try {
                Method m = miPapi.getClass().getMethod("mostrarResumen", null);

                m.invoke(miPapi);
                m.setAccessible(true);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }


    private static void abrirDoc(View view, ItemDocs it) {
        Intent inte = new Intent();
        inte.putExtra("IdDoc", it.getTipo());
        inte.putExtra("Numero", it.getNumero());
        inte.setClass(view.getContext(), MiDoc.class);
        view.getContext().startActivity(inte);
    }

    class SubirDoc extends AsyncTask<Void, String, Boolean> {
        Context ctx;
        boolean pdf;
        String pathPdf = "";
        ItemDocs item;

        SubirDoc(View v, boolean conPdf, ItemDocs it) {
            ctx = v.getContext();
            pdf = conPdf;
            item = it;
            Opciones.setErr(Opciones.Errores.ERROR_TODO_OK.getValor());
        }

        Nube miNube = new Nube();

        @Override
        protected Boolean doInBackground(Void... params) {
            publishProgress(ctx.getString(R.string.lb_compruebacnx));
            miNube.callIt(Nube.CallWebServ.CALL_TESTCNX);
            if (!Util.mostrarError(ctx)) return false;
            publishProgress(ctx.getString(R.string.lb_compruebaCont));
            miNube.callIt(Nube.CallWebServ.CALL_CONTADORES);
            if (!Util.mostrarError(ctx)) return false;
            if (item.getTocado()) {
                publishProgress(ctx.getString(R.string.lb_enviandoArts));
                miNube.subirTabla("ARTICULOS");
                if (!Util.mostrarError(ctx)) return false;
                publishProgress(ctx.getString(R.string.lb_enviandoDocs));
                miNube.subirDocs(item.getId());
                if (!Util.mostrarError(ctx)) return false;
            }

            if (pdf) {
                publishProgress(ctx.getString(R.string.lb_descargaPdf));
                miNube.descargarPDF(item.getNumero(), item.getTipo(), ctx);
                if (!Util.mostrarError(ctx)) return false;
            }
            return true;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            Util.mostrarProgreso(values[0], true, ctx);
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            Util.mostrarProgreso("", false, ctx);
            if (!success) {
                if (Opciones.getErr().equals(Opciones.Errores.ERROR_TEST_FAIL))
                    ctx.startActivity(new Intent(ctx, LoginActivity.class));
                else
                    Util.msgBox(ctx.getString(R.string.lb_errorCnx), Opciones.getErr().getDescripcion(), ctx);
            }
            mostrarResumen();
        }

        @Override
        protected void onCancelled() {
            Util.mostrarProgreso("", false, ctx);
        }
    }
}
