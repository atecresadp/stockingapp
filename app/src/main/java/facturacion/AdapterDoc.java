package facturacion;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.atecresa.www.myware.R;
import com.atecresa.www.myware.Util;

import java.util.ArrayList;


/**
 * Created by Gustavo on 18/12/2016.
 */

public class AdapterDoc extends RecyclerView.Adapter<AdapterDoc.ViewHolder> {

    private ArrayList<ItemDoc> items;


    public AdapterDoc(ArrayList<ItemDoc> items) {
        this.items = items;
    }

    ;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_doc, parent, false);
        // set the view's size, margins, paddings and layout parameters

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ItemDoc it = items.get(position);
        holder.mNomArt.setText(it.getNombre());
        holder.mCodigo.setText(it.getCodigo());
        holder.mUnidades.setText(Util.formatearUnidades(it.getUnidades()));
        holder.mSuma.setText(it.getSumaUnid());
        holder.mObs.setText(it.getObs());
        if (it.getObs().equals(""))
            holder.mObs.setVisibility(View.GONE);
        else
            holder.mObs.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void clear() {
        int size = this.items.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.items.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mUnidades, mCodigo, mNomArt, mSuma, mObs;

        public ViewHolder(View v) {
            super(v);
            mUnidades = (TextView) v.findViewById(R.id.itemUnids);
            mCodigo = (TextView) v.findViewById(R.id.itemCodigo);
            mNomArt = (TextView) v.findViewById(R.id.itemNomArt);
            mSuma = (TextView) v.findViewById(R.id.itemSuma);
            mObs = (TextView) v.findViewById(R.id.itemObs);
        }
    }


}
