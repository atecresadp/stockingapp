package com.atecresa.www.myware;

import android.Manifest;
import android.app.DownloadManager;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.CharBuffer;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import SqLite.Cnx;

import static com.atecresa.www.myware.Opciones.setErr;

/**
 * Created by Gustavo on 29/12/2016.
 */

public class Nube {

    private String sucesos = "";
    private int numSucesos = 0;

    private ArrayList<Empresa> empresas;

    public Nube() {
    }

    private enum Linkes {
        LNK_ALMACENES("ALMACENES,ALMACENES", "ID,CODIGO,DESCRIPCION", "ID,CODIGO,NOMBRE"),
        LNK_ARTICULOS("ARTICULOS,ARTICULOS", "ID,CODIGO,DESCRIPCION,IDFAMILIA,TIPO", "ID,CODIGO,NOMBRE,IDFAMILIA,TIPO"),
        LNK_RELACIONES("RELACIONES,RELACIONES", "ID,IDARTICULO,CODEXTERNO,IDCLIENTEPROVEEDOR", "ID,IDARTICULO,CODEXTERNO,IDPROVEEDOR"),
        LNK_CADUCIDAD("CADUCIDAD,CADUCIDAD", "ID,IDARTICULO,FECHACADUCIDAD,NLOTE", "ID,IDARTICULO,FECHACADUCIDAD,NLOTE"),
        LNK_CLIENTEPROVEEDOR("CLIENTEPROVEEDOR,CLIPROV", "ID,CODIGO,NOMBRE,TIPO", "ID,CODIGO,NOMBRE,TIPO"),
        LNK_FAMILIAS("FAMILIAS,FAMILIAS", "ID,DESCRIPCION,NCADUCIDAD", "ID,NOMBRE,NCADUCIDAD"),
        LNK_TIPOINCIDENCIA("TIPOINCIDENCIA,TIPO_INC", "ID,DESCRIPCION", "ID,DESCRIPCION"),
        LNK_PERSONAL("PERSONAL,USUARIOS", "ID,USUARIO", "ID,NOMBRE");

        String tablaOrigen;
        String tablaDestino;
        String camposOrigen;
        String camposDestino;

        Linkes(String tablas, String camposO, String camposD) {
            tablaOrigen = tablas.split(",")[0];
            tablaDestino = tablas.split(",")[1];
            camposOrigen = camposO;
            camposDestino = camposD;
        }

        public String getTablaOrigen() {
            return tablaOrigen;
        }

        public String getTablaDestino() {
            return tablaDestino;
        }

        public Collection<String> getCamposOrigen() {
            return Arrays.asList(camposOrigen.split(","));
        }

        public Collection<String> getCamposDestino() {
            return Arrays.asList(camposDestino.split(","));
        }

        public String getJoinCamposDest() {
            return camposDestino;
        }
    }

    public enum CallWebServ { // Con esto se estructura las llamadas a los servicios        
        CALL_TESTCNX("", "TEST", null, null),
        CALL_LOGIN_INI("", "SETLOGIN", "{'EMAIL':'GETEMAIL','PWD':'GETPWD','USARTERMINALPROPIO':true,'TIPO':'STOCKING'}", null),
        CALL_LOGIN("", "SETLOGIN", "{'EMAIL':'GETEMAIL','PWD':'GETPWD','LICENCIA':'GETLICENCIA','UUID':'GETUUID','USARTERMINALPROPIO':true,'TIPO':'STOCKING'}", null),
        CALL_CONTADORES("FACTURACION", "GETCONTADORES", "{'IDCONTADOR':1,'IDTPV':GETIDTERMINAL}", null),
        CALL_ARTICULOS("EXPORTACIONGENERICA", "GETTABLA", "{'TABLA':'ARTICULOS','ORDEN':'ID'}", Linkes.LNK_ARTICULOS),
        CALL_RELACIONES("EXPORTACIONGENERICA", "GETTABLA", "{'TABLA':'RELACIONES','ORDEN':'ID'}", Linkes.LNK_RELACIONES),
        CALL_ALMACENES("EXPORTACIONGENERICA", "GETTABLA", "{'TABLA':'ALMACENES','ORDEN':'ID'}", Linkes.LNK_ALMACENES),
        CALL_CADUCIDAD("EXPORTACIONGENERICA", "GETTABLA", "{'TABLA':'CADUCIDAD','ORDEN':'ID'}", Linkes.LNK_CADUCIDAD),
        CALL_CLIENTEPROVEEDOR("EXPORTACIONGENERICA", "GETTABLA", "{'TABLA':'CLIENTEPROVEEDOR','ORDEN':'ID'}", Linkes.LNK_CLIENTEPROVEEDOR),
        CALL_FAMILIAS("EXPORTACIONGENERICA", "GETTABLA", "{'TABLA':'FAMILIAS','ORDEN':'ID'}", Linkes.LNK_FAMILIAS),
        CALL_TIPOINCIDENCIA("EXPORTACIONGENERICA", "GETTABLA", "{'TABLA':'TIPOINCIDENCIA','ORDEN':'ID'}", Linkes.LNK_TIPOINCIDENCIA),
        CALL_PERSONAL("EXPORTACIONGENERICA", "GETTABLA", "{'TABLA':'PERSONAL','ORDEN':'ID'}", Linkes.LNK_PERSONAL);

        JsonObject base;
        Linkes links;
        String params;
        String funcion;
        String alias = "";

        CallWebServ(String modulo, String funcion, String param, Linkes rela) {
            this.funcion = funcion;
            base = jSonBase(modulo, funcion);
            params = param;
            links = rela;
            if (rela != null) alias = rela.getTablaOrigen();
        }

        public Linkes getLinks() {
            return links;
        }

        public JsonObject getJsonBase() {
            return base;
        }

        public String getParams() {
            return params;
        }

        public String getFuncion() {
            return funcion;
        }

        public String getAlias() {
            return Nube.getAlias(alias);
        }
    }

    public void callIt(final CallWebServ llamada) {
        setErr(Opciones.Errores.ERROR_TODO_OK.getValor());

        if (!Util.tengoInternet()) return;
        JsonReader reader = leerJsReader(llamada);
        try {
            switch (llamada) {
                //region Contadores
                case CALL_CONTADORES:
                    boolean esBueno = false;
                    int tipo = 0;
                    StringBuilder sql = new StringBuilder();
                    reader.beginObject();

                    if (reader.nextName().equals("RS")) {
                        reader.beginArray();
                        while (reader.hasNext()) {
                            reader.beginObject();
                            esBueno = false;
                            while (reader.hasNext()) {
                                switch (reader.nextName()) {
                                    case "ID":
                                        tipo = reader.nextInt();
                                        esBueno = (Opciones.TipoDoc.getById(tipo) != null);
                                        break;
                                    case "NUMERO":
                                        if (esBueno) {
                                            int objValor = reader.nextInt() + 1;
                                            String c = Opciones.TipoDoc.getById(tipo).getNomCampo();
                                            sql.append("UPDATE CONTADORES SET " + c + "=" + objValor +
                                                    " WHERE ID = 1 AND " + c + "<" + objValor + "\r\n");
                                        } else
                                            reader.skipValue();
                                        break;
                                    default:
                                        reader.skipValue();
                                        break;
                                }
                            }
                            reader.endObject();
                        }
                        reader.endArray();
                    }
                    if (sql.length() > 0)
                        Cnx.SqlMasivo(sql.toString());
                    break;
                //endregion

                //region Test de conexión
                case CALL_TESTCNX: {
                    reader.beginObject();
                    while (reader.hasNext()) {
                        switch (reader.nextName()) {
                            case "ERROR":
                                setErr(Opciones.Errores.ERROR_TEST_FAIL.getValor());
                                reader.skipValue();
                                break;
                            default:
                                reader.skipValue();
                        }
                    }
                    reader.endObject();
                    break;
                }
                //endregion

                //region LogIn
                case CALL_LOGIN_INI:
                case CALL_LOGIN: {
                    empresas = new ArrayList<Empresa>();
                    boolean failLog = true;
                    String a;
                    setErr(Opciones.Errores.ERROR_LOGIN_FAIL.getValor());
                    reader.beginObject();
                    while (reader.hasNext() && failLog) {
                        a = reader.nextName();
                        switch (a) {
                            case "ERROR":
                                int er = reader.nextInt();
                                switch (er){
                                    case 2006:
                                        setErr(Opciones.Errores.ERROR_DEMASIADOS_INTENTOS.getValor());
                                        break;
                                }
                                break;
                            case "SESION":
                                reader.beginObject();
                                while (reader.hasNext()) {
                                    switch (reader.nextName()) {
                                        case "TERMINAL":
                                            reader.beginObject();
                                            while (reader.hasNext()) {
                                                switch (reader.nextName()) {
                                                    case "ID":
                                                        Opciones.setIdTerminal(String.valueOf(reader.nextInt()));
                                                        break;
                                                    case "DESCRIPCION":
                                                        Opciones.setNomTerminal(reader.nextString());
                                                        break;
                                                    default:
                                                        reader.skipValue();
                                                        break;
                                                }
                                            }
                                            reader.endObject();
                                            break;
                                        case "EMPRESA":
                                            reader.beginObject();
                                            while (reader.hasNext()) {
                                                switch (reader.nextName()) {
                                                    case "UUID":
                                                        Opciones.setUuid(reader.nextString());
                                                        failLog = false;
                                                        break;
                                                    case "NOMBRE":
                                                        Opciones.setNombreEmpresa(reader.nextString());
                                                        break;
                                                    default:
                                                        reader.skipValue();
                                                }
                                            }
                                            reader.endObject();
                                            break;
                                        case "OPERADOR":
                                            reader.beginObject();
                                            while (reader.hasNext()) {
                                                switch (reader.nextName()) {
                                                    case "ID":
                                                        Opciones.setIdUsuario(reader.nextInt());
                                                        break;
                                                    case "USUARIO":
                                                        Opciones.setNombreUsuario(reader.nextString());
                                                        break;
                                                    default:
                                                        reader.skipValue();
                                                }
                                            }
                                            reader.endObject();
                                            break;
                                        case "INFOLICENCIA":
                                            reader.beginObject();
                                            while (reader.hasNext()) {
                                                a = reader.nextName();
                                                switch (a) {
                                                    case "VALIDA":
                                                        if (reader.nextInt() == 0)
                                                            Opciones.setErr(Opciones.Errores.ERROR_LICENCIA_INACTIVA.getValor());
                                                        break;
                                                    case "LICENCIA":
                                                        Date fechaServ = Util.ahora();
                                                        reader.beginObject();
                                                        while (reader.hasNext()) {
                                                            a = reader.nextName();
                                                            switch (a) {
                                                                case "LICENCIA":
                                                                    Opciones.setLicencia(reader.nextString());
                                                                    break;
                                                                case "FECHASISTEMA":
                                                                    fechaServ = Util.formatFecha(reader.nextString(), false);
                                                                    break;
                                                                case "ACTIVACION":
                                                                    if (Util.formatFecha(reader.nextString(), false).after(fechaServ))
                                                                        setErr(Opciones.Errores.ERROR_LICENCIA_FUERA_FECHA.getValor());
                                                                    break;
                                                                case "CADUCIDAD":
                                                                    a = reader.nextString();
                                                                    Opciones.setCaducidad(Util.formatFecha(a, false));
                                                                    break;
                                                                case "CADUCA":
                                                                    if (reader.nextBoolean()) {
                                                                        setErr(Opciones.Errores.ERROR_LICENCIA_CADUCA.getValor());
                                                                        break;
                                                                    }
                                                                case "DIASCADUCIDAD":
                                                                    if (reader.nextInt() <= 7)
                                                                        setErr(Opciones.Errores.ERROR_LICENCIA_PROXIMA_CADUCAR.getValor());
                                                                    break;

                                                                default:
                                                                    reader.skipValue();
                                                                    break;
                                                            }
                                                        }
                                                        reader.endObject();
                                                        break;
                                                    default:
                                                        reader.skipValue();
                                                }
                                            }
                                            reader.endObject();
                                            break;
                                        default:
                                            reader.skipValue();
                                            break;
                                    }
                                }
                                reader.endObject();
                                break;
                            case "EMPRESAS":
                                reader.beginArray();
                                Empresa e = null;
                                while (reader.hasNext()) {
                                    reader.beginObject();
                                    e = new Empresa();
                                    while (reader.hasNext()) {
                                        switch (reader.nextName()) {
                                            case "IDOPERADOR":
                                                e.IdUsuario = reader.nextInt();
                                                break;
                                            case "EMPRESA":
                                                e.Nombre = reader.nextString();
                                                break;
                                            case "OPERADOR":
                                                e.NombreUsuario = reader.nextString();
                                                break;
                                            case "CODIGO":
                                                e.CodigoUsuario = reader.nextString();
                                                break;
                                            case "UUID":
                                                e.Uuid = reader.nextString();
                                                break;
                                            default:
                                                reader.skipValue();
                                        }
                                    }
                                    reader.endObject();
                                    if (e != null) empresas.add(e);
                                }
                                reader.endArray();
                                break;
                            default:
                                reader.skipValue();
                                break;
                        }

                    }
                    if ((!failLog && Opciones.getLicencia().length()>0) || (empresas.size() > 0) && failLog)
                        Opciones.setErr(Opciones.Errores.ERROR_TODO_OK.getValor());
                    break;
                }
                //endregion

                //region Descarga tablas mantenimiento
                case CALL_ARTICULOS:
                case CALL_ALMACENES:
                case CALL_PERSONAL:
                case CALL_CLIENTEPROVEEDOR:
                case CALL_FAMILIAS:
                case CALL_TIPOINCIDENCIA:
                case CALL_CADUCIDAD:
                case CALL_RELACIONES: {
                    String objName = "";
                    String objValue = "";
                    SQLiteDatabase db = Cnx.beginTran();
                    String tblDestino = llamada.links.getTablaDestino();
                    Cnx.sqlMasivo("DELETE FROM " + tblDestino, db);
                    String camposD = llamada.links.getJoinCamposDest();
                    Collection<String> camposO = llamada.links.getCamposOrigen();
                    reader.beginObject();
                    objName = reader.nextName();
                    if (objName.equals("RS")) {
                        reader.beginArray();
                        while (reader.hasNext()) {
                            reader.beginObject();
                            ContentValues valSql = new ContentValues();
                            while (reader.hasNext()) {
                                objName = reader.nextName();
                                objValue = reader.nextString();
                                if (camposO.contains(objName))
                                    valSql.put(objName, objValue);
                            }

                            StringBuilder consulta = new StringBuilder();
                            consulta.append("INSERT INTO ").append(tblDestino).append(" (").append(camposD).append(") VALUES (");

                            for (String it : camposO) {
                                consulta.append("'").append(valSql.getAsString(it).replace("'", "´")).append("',");
                            }
                            consulta.delete(consulta.length() - 1, consulta.length());
                            consulta.append(")");

                            Cnx.sqlMasivo(consulta.toString(), db);
                            reader.endObject();
                        }
                        reader.endArray();
                    }
                    Cnx.commitTran(db);
                    sucesos = sucesos + getAlias(tblDestino) + ": " + Cnx.Buscar("SELECT COUNT(0) FROM " + tblDestino, "0") + "\n";
                    break;
                }
                //endregion

            }
        } catch (Exception e) {
            e.printStackTrace();
            if (Opciones.getErr().equals(Opciones.Errores.ERROR_TODO_OK))
                setErr(Opciones.Errores.ERROR_FALLO_CONEXION.getValor());
            try {
                if (reader != null) reader.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        try {
            if (reader != null) reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    ArrayList<Empresa> getEmpresas() {
        return empresas;
    }

    //region ENVÍOS

    public void subirDocs(int idDoc) {
        setErr(Opciones.Errores.ERROR_TODO_OK.getValor());
        if (!Util.tengoInternet()) return;
        JsonObject parametros = new JsonObject();
        JsonObject documento = new JsonObject();
        JsonArray linArr = new JsonArray();
        JsonObject cabecera = new JsonObject();
        String aux = " AND MAESTRO.ID = " + idDoc;
        if (idDoc == -1) aux = "";
        int nuevaCab = -1;
        int i = 1;

        Cursor rs = Cnx.exeSQL("SELECT MAESTRO.ID, MAESTRO.FECHA, MAESTRO.NUMERO, MAESTRO.IDCLIPROV, MAESTRO.TIPO, MAESTRO.OBS MOBS, DETALLE.UNID, " +
                "MAESTRO.IDALM, MAESTRO.IDALMDEST, (SELECT CODIGO FROM ARTICULOS WHERE ID = DETALLE.IDARTICULO) CODART, DET_OBS.OBS DOBS " +
                "FROM MAESTRO LEFT JOIN DETALLE ON DETALLE.IDMAESTRO = MAESTRO.ID LEFT JOIN DET_OBS ON DETALLE.ID = DET_OBS.IDDETALLE " +
                "WHERE UUID = '" + Opciones.getUuid() + "' AND MAESTRO.TOCADO = 1" + aux);

        if (rs.moveToFirst()) {
            numSucesos = 0;
            do {
                if (nuevaCab != rs.getInt(rs.getColumnIndex("ID"))) {
                    if (nuevaCab != -1) {
                        documento.add("LINEAS", linArr);
                        documento.add("CABECERA", cabecera);
                        parametros.add("DOCUMENTO", documento);
                        subir(parametros, nuevaCab);
                        parametros = new JsonObject();
                        documento = new JsonObject();
                        linArr = new JsonArray();
                        cabecera = new JsonObject();
                        numSucesos++;
                    }
                    parametros.addProperty("VERMODIFICACIONES", false);
                    documento.addProperty("SOBREESCRIBIR", true);
                    documento.addProperty("MODOIMPORTACION", false);
                    documento.addProperty("NUMERO", rs.getString(rs.getColumnIndex("NUMERO")));
                    documento.addProperty("IDTPV", Opciones.getIdTerminal());
                    documento.addProperty("IDCONTADOR", 1);
                    documento.addProperty("TIPO", rs.getString(rs.getColumnIndex("TIPO")));
                    documento.addProperty("ALMACENORIGEN", rs.getInt(rs.getColumnIndex("IDALM")));
                    documento.addProperty("ALMACENDESTINO", rs.getInt(rs.getColumnIndex("IDALMDEST")));

                    cabecera.addProperty("IDCLIENTEPROVEEDOR", rs.getString(rs.getColumnIndex("IDCLIPROV")));
                    cabecera.addProperty("NUMERO", rs.getString(rs.getColumnIndex("NUMERO")));
                    cabecera.addProperty("IDTPV", Opciones.getIdTerminal());
                    cabecera.addProperty("IDCONTADOR", 1);
                    try {
                        cabecera.addProperty("FECHA", Util.formatearFecha(rs.getString(rs.getColumnIndex("FECHA"))));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (rs.getString(rs.getColumnIndex("MOBS")) != null)
                        cabecera.addProperty("OBS", rs.getString(rs.getColumnIndex("MOBS")));
                    nuevaCab = rs.getInt(rs.getColumnIndex("ID"));
                }
                JsonObject linea = new JsonObject();
                if (rs.getString(rs.getColumnIndex("CODART")) != null) {
                    linea.addProperty("CODIGOARTICULO", rs.getString(rs.getColumnIndex("CODART")));
                    linea.addProperty("UDS", rs.getDouble(rs.getColumnIndex("UNID")));
                    if (rs.getString(rs.getColumnIndex("DOBS")) != null)
                        linea.addProperty("OBSERVACION", rs.getString(rs.getColumnIndex("DOBS")));
                    linArr.add(linea);
                }
            } while (rs.moveToNext());
            documento.add("CABECERA", cabecera);
            documento.add("LINEAS", linArr);
            parametros.add("DOCUMENTO", documento);
            subir(parametros, nuevaCab);
            numSucesos++;
            sucesos = sucesos + "Documentos enviados: " + numSucesos + "\n";
        }
    }

    //Sube los datos de un maestro concreto con sus líneas
    private void subir(JsonObject doc, int idMaestro) {
        HTTP_Con cnx = new HTTP_Con();
        JsonObject res = jSonBase("FACTURACION", "SETDOC");
        res.add("P", doc);
        JsonParser parser = new JsonParser();
        try {
            res = (JsonObject) parser.parse(cnx.postear(res.toString()));
        } catch (Exception ex) {
            res = new JsonObject();
        }
        boolean todoOk = res.has("RESULT");
        if (todoOk) todoOk = res.get("RESULT").getAsBoolean();
        if (todoOk) {
            Cnx.exeUpdSQL("UPDATE MAESTRO SET TOCADO = 0 WHERE ID = " + idMaestro);
        } else {
            Util.miError("Subir Doc", res.toString());
            sucesos = sucesos + "Servidor no responde: " +
                    Opciones.TipoDoc.getById(Integer.parseInt(Cnx.Buscar("SELECT TIPO FROM MAESTRO WHERE ID = " + idMaestro, "0"))).getNombre() + " " + idMaestro + "\n";
            numSucesos--;
        }
    }

    //Aquí subimos una tabla concreta que haya sido "tocada"
    public void subirTabla(String tabla) {
        setErr(Opciones.Errores.ERROR_TODO_OK.getValor());
        if (!Util.tengoInternet()) return;
        Cursor rs = Cnx.exeSQL("SELECT ID, " + tabla + ".* FROM " + tabla + " WHERE TOCADO = 1");
        if (rs.moveToFirst()) {
            JsonObject campos = new JsonObject();
            JsonArray mDependencia = new JsonArray();
            JsonObject dependencias = new JsonObject();
            JsonObject regs = new JsonObject();
            JsonObject registros = new JsonObject();
            JsonObject origen = new JsonObject();
            JsonObject destino = new JsonObject();
            numSucesos = 0;
            do {
                    switch (tabla) {
                        case "ARTICULOS":
                            campos.addProperty("CODIGO", rs.getString(rs.getColumnIndex("CODIGO")));
                            campos.addProperty("DESCRIPCION", rs.getString(rs.getColumnIndex("NOMBRE")));
                            campos.addProperty("NOMMAQUINA", rs.getString(rs.getColumnIndex("NOMBRE")));
                            campos.addProperty("COSTEMEDIO", rs.getDouble(rs.getColumnIndex("COSTO")));
                            campos.addProperty("COSTENETO", rs.getDouble(rs.getColumnIndex("COSTO")));
                            campos.addProperty("IDFAMILIA", rs.getInt(rs.getColumnIndex("IDFAMILIA")));

                            origen.addProperty("TABLA", "ARTICULOS");
                            origen.addProperty("NOMBRE", "ID");

                            dependencias.add("ORIGEN", origen);

                            destino.addProperty("TABLA", "PRECIOS");
                            destino.addProperty("NOMBRE", "IDARTICULO");

                            dependencias.add("DESTINO", destino);

                            registros.addProperty("TABLA", "PRECIOS");

                            regs.addProperty("IDNOMBRE", 1);
                            regs.addProperty("PVP", rs.getDouble(rs.getColumnIndex("PVP")));

                            registros.add("CAMPOS", regs);

                            dependencias.add("REGISTROS", registros);
                            mDependencia.add(dependencias);
                            break;
                    }
                subir(tabla, campos, mDependencia,rs.getInt(rs.getColumnIndex("ID")), false);
                numSucesos++;
            } while (rs.moveToNext());
            sucesos = sucesos + tabla + " enviados: " + numSucesos + "\n";
        }
    }

    public void subirFichero(File f){
        setErr(Opciones.Errores.ERROR_TODO_OK.getValor());
        if (!Util.tengoInternet()) return;
        JsonObject campos = new JsonObject();
        campos.addProperty("FOTO", Util.getStringFile(f));
        campos.addProperty("FICHERO", f.getAbsolutePath().toString());
        campos.addProperty("IDTIPO", "5");
        subir("IMAGENES", campos, null, 0, true);
    }

    //Esto se usa para subir un registro concreto de una tabla de mantenimiento
    private void subir(String tabla, JsonObject campos, JsonArray dependencias, int tag, boolean sobreescribir) {

        JsonObject res = jSonBase("MANTENIMIENTO", "SETREGISTRO");
        JsonObject parr = new JsonObject();
        HTTP_Con cnx = new HTTP_Con();
        parr.addProperty("TABLA", tabla);
        parr.addProperty("SOBREESCRIBIR", sobreescribir);
        if (tag>0) parr.addProperty("TAG", tag);
        parr.add("CAMPOS", campos);

        res.add("P", parr);
        if (dependencias != null) res.add("DEPENDENCIAS", dependencias);
        JsonParser parser = new JsonParser();
        res = (JsonObject) parser.parse(cnx.postear(res.toString()));

        if (res.has("TAG")) {
            if (!res.get("TAG").getAsString().equals(""))
                Cnx.exeUpdSQL("UPDATE " + tabla + " SET TOCADO = 0 WHERE ID = " + res.get("TAG").getAsString());
        }
    }

    //endregion

    //region FUNCIONES DE APOYO

    void conectarEmpresa(int pos) {

        try {
            Empresa e = empresas.get(pos);
            Opciones.setUuid(e.Uuid);
            Opciones.setNombreEmpresa(e.Nombre);
            Opciones.setIdUsuario(e.IdUsuario);
            Opciones.setCodigoUsuario(e.CodigoUsuario);
            Opciones.setNombreUsuario(e.NombreUsuario);
        } catch (Exception e1) {
            Util.miError("Conectando Empresa", e1.getMessage());
            e1.printStackTrace();
        }
    }

    static String getAlias(String nomTbl) {
        if (nomTbl.equals("TIPO_INC")) nomTbl = "TIPO INCIDENCIAS";
        if (nomTbl.equals("CLIPROV")) nomTbl = "CLIENTES/PROVEEDORES";
        if (nomTbl.equals("RELACIONES")) nomTbl = "CÓDIGOS EXTERNOS";
        return nomTbl;
    }

    String getSucesos() {
        return sucesos;
    }

    private class HTTP_Con {
        String ejecutar(String json) {
            StringBuilder resultado = null;

            try {
                URL url;

                url = new URL(Opciones.getUrlServidor() + encode(json));
                Log.d("Url", Opciones.getUrlServidor() + encode(json));
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                BufferedReader r = new BufferedReader(new InputStreamReader(in,
                        Opciones.Constantes.Character_Set));

                resultado = new StringBuilder();
                CharBuffer sb = CharBuffer.allocate(512);

                while (r.read(sb) > 0) {
                    sb.flip();
                    resultado.append(sb.toString());
                    sb.clear();
                }
                r.close();

                urlConnection.disconnect();

            } catch (Exception e) {
                Util.miError("Nube", e.getMessage());
            }
            return resultado != null ? resultado.toString() : null;
        }

        String postear(String json) {
            HttpURLConnection con = null;
            String resultado = null;

            try {
                URL url = new URL(Opciones.getUrlServidor());
                //Log.d("Url", Opciones.getUrlServidor());
                // Construir los datos a enviar
                String data = "JSON=" + URLEncoder.encode(json, "UTF-8");

                con = (HttpURLConnection) url.openConnection();

                con.setRequestMethod("POST");
                con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                con.setRequestProperty("Content-Length", "" + Integer.toString(data.getBytes().length));
                //con.setRequestProperty("Content-Language", "es-SP");

                con.setUseCaches(false);
                con.setDoInput(true);
                con.setDoOutput(true);

                DataOutputStream out = new DataOutputStream(con.getOutputStream());

                out.writeBytes(data);
                out.flush();
                out.close();

                // get response
                InputStream responseStream = con.getInputStream();
                BufferedReader responseStreamReader = new BufferedReader(new InputStreamReader(responseStream));
                String line = "";
                StringBuilder stringBuilder = new StringBuilder();
                while ((line = responseStreamReader.readLine()) != null) {
                    stringBuilder.append(line);
                }
                responseStreamReader.close();
                resultado = stringBuilder.toString();

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (con != null)
                    con.disconnect();
            }
            return resultado != null ? resultado.toString() : null;
        }
    }

    private static String encode(String cad) {
        try {
            return URLEncoder.encode(cad, Opciones.Constantes.Character_Set);
        } catch (UnsupportedEncodingException e) {
            Util.miError("Encode", e.getMessage());
            return "";
        }
    }

    JsonObject getJsonBase(CallWebServ llamada) {
        JsonObject parametros = null;
        JsonObject base = null;

        try {
            String par = llamada.getParams();
            if (par != null) {
                if (!llamada.getFuncion().equals("GETTABLA")) {
                    par = par.replace("GETEMAIL", Opciones.getEmailUsuario());
                    par = par.replace("GETPWD", Opciones.getPasswordUsuario());
                    par = par.replace("GETUUID", Opciones.getUuid());
                    par = par.replace("GETIDTERMINAL", String.valueOf(Opciones.getIdTerminal()));
                    par = par.replace("GETLICENCIA", Opciones.getLicencia());
                    par = par.replace("GETIDDISPOSITIVO", Opciones.getIdDispositivo());
                    par = par.replace("GETNOMBREEMPRESA", Opciones.getNombreEmpresa());
                }
                parametros = (JsonObject) new JsonParser().parse(par);
                if (llamada.getLinks() != null)
                    parametros.addProperty("SELECCION", llamada.getLinks().camposOrigen);
            }
            base = llamada.getJsonBase();

            if (parametros != null) base.add("P", parametros);
        } catch (Exception e) {
            Util.miError("JsonBase", e.getMessage());
            e.printStackTrace();
        }
        return base;
    }

    static JsonObject jSonBase(String modulo, String funcion) {
        JsonObject b = new JsonObject();
        try {
            b.addProperty("SESION", Opciones.getIdDispositivo());
            b.addProperty("F", funcion);
            if (!modulo.equals("")) b.addProperty("M", modulo);
        } catch (Exception e) {
            Util.miError("JsonBase", e.getMessage());
            e.printStackTrace();
        }
        return b;
    }

    //endregion

    private JsonReader leerJsReader(CallWebServ llamada) {
        JsonObject peticion = getJsonBase(llamada);
        try {
            BufferedReader bufferedReader = getBuffer(peticion.toString());
            return new JsonReader(bufferedReader);
        } catch (Exception ex) {
            setErr(Opciones.Errores.ERROR_FALLO_CONEXION.getValor());
            Util.miError("Nube", ex.getMessage());
            return null;
        }
    }

    private BufferedReader getBuffer(String peticion) {
        try {
            URL url = new URL(Opciones.getUrlServidor() + java.net.URLEncoder.encode(peticion, "UTF-8"));
            return new BufferedReader(new InputStreamReader(url.openStream()));
        } catch (Exception ex) {
            setErr(Opciones.Errores.ERROR_FALLO_CONEXION.getValor());
            Util.miError("Nube", ex.getMessage());
            return null;
        }
    }

    public void descargarPDF(int numero, Opciones.TipoDoc tipo, Context ctx) {
        setErr(Opciones.Errores.ERROR_TODO_OK.getValor());
        if (!Util.getTengoPermiso(Manifest.permission.WRITE_EXTERNAL_STORAGE, ctx)) return;

        try {
            JsonObject jo = new JsonObject();
            jo.addProperty("SESION", Opciones.getIdDispositivo());
            jo.addProperty("OBJETO", "PDF");
            jo.addProperty("NUMERO", numero);
            jo.addProperty("TIPO", tipo.getValor());
            jo.addProperty("IDCONTADOR", 1);
            jo.addProperty("IDTPV", Opciones.getIdTerminal());
            String url = Opciones.urlServidorPdf() + java.net.URLEncoder.encode(jo.toString(), "UTF-8");
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
            request.setDescription("Descargando " + tipo.getNombre());
            request.setTitle(tipo.getNombre() + " nº " + numero);
            // in order for this if to run, you must use the android 3.2 to compile your app
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            }
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, tipo.getNombre() + "_" + numero + ".pdf");

            // get download service and enqueue file
            DownloadManager manager = (DownloadManager) ctx.getSystemService(Context.DOWNLOAD_SERVICE);
            manager.enqueue(request);
        } catch (Exception e) {
            Util.miError("Decargando PDF", e.getMessage());
            e.printStackTrace();
        }
    }

    public void descargarUpd(Context ctx) {
        setErr(Opciones.Errores.ERROR_TODO_OK.getValor());
        if (!Util.getTengoPermiso(Manifest.permission.WRITE_EXTERNAL_STORAGE, ctx)) return;

        try {

            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(Opciones.Constantes.getUrlDescarga));
            request.setDescription("Descargando actualización");
            request.setTitle("StockingApp");
            // in order for this if to run, you must use the android 3.2 to compile your app
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            }
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "StockingApp.apk");

            // get download service and enqueue file
            DownloadManager manager = (DownloadManager) ctx.getSystemService(Context.DOWNLOAD_SERVICE);
            manager.enqueue(request);
        } catch (Exception e) {
            Util.miError("Decargando actualización", e.getMessage());
            e.printStackTrace();
        }
    }

    class Empresa {
        int IdUsuario = -1;
        String Nombre = "";
        String CodigoUsuario = "";
        String NombreUsuario = "";
        String Uuid = "";

        Empresa() {
        }
    }
}