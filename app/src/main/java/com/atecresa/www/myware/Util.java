package com.atecresa.www.myware;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.ToneGenerator;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Base64OutputStream;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import facturacion.ItemDocs;
import facturacion.Maestro;

import static android.content.Context.ACTIVITY_SERVICE;
import static com.atecresa.www.myware.Opciones.TipoDoc.TRASPASO;

/*import pk.sqlite.Cnx;
import com.atecresa.conexiones.ClienteSocket;
import com.atecresa.fiscalvia.R;*/

/**
 * Created by Gustavo on 13/12/2016.
 */


public final class Util {

    public static Opciones.GrupoDocs grupo_Activo; //Va del 0 al 3. Identifica la opción pulsada (Ventas, compras o Almacén)

    private static Context ctx;
    private static final java.text.DecimalFormat formatEuro = new java.text.DecimalFormat("0.00 €");
    private static final java.text.DecimalFormat formatUnids = new java.text.DecimalFormat("0.##");
    private static final java.text.DecimalFormat formatUnidsSgn = new java.text.DecimalFormat("+0.##;-0.##");
    private static final String formatFecha = "dd/MM/yyyy HH:mm:ss";
    private static final String formatFechaSqlite = "yyyy-MM-dd HH:mm:ss";

    public static Object noNulo(Object p) {

        if (p != null) {
            return (p);
        } else {
            return "";
        }
    }


    public static Object noNulo(Object p, Object porDefecto) {

        if (p != null) {
            return (p);
        } else {
            return porDefecto;
        }
    }

    public static String hoy() {
        Date dom = ahora();
        SimpleDateFormat formatoDeFecha = new SimpleDateFormat(formatFecha, Locale.getDefault());
        return formatoDeFecha.format(dom);
    }

    public static Date ahora() {
        Calendar cal = Calendar.getInstance();
        //cal.set(2013, 5, 13, 14, 28, 30);
        Date dom = cal.getTime();

        return dom;
    }

    public static String formatearFecha(Date dom) {
        SimpleDateFormat formatoDeFecha = new SimpleDateFormat(formatFecha, Locale.getDefault());
        return formatoDeFecha.format(dom);
    }

    public static String formatearFecha(String valor, String formatoIni, String formatoFin) throws java.text.ParseException {
        String aux = null;
        Date convertDate = new Date();
        try {
            SimpleDateFormat formatoIniFecha = new SimpleDateFormat(formatoIni);
            convertDate = formatoIniFecha.parse(valor);
            SimpleDateFormat formatoFinFecha = new SimpleDateFormat(formatoFin);
            aux = formatoFinFecha.format(convertDate);
        } catch (ParseException e) {
            aux = "";
        }
        return aux;
    }

    public static String formatearFecha(String valor) throws java.text.ParseException {
        String aux = null;
        Date convertDate = new Date();
        try {
            SimpleDateFormat formatoIniFecha = new SimpleDateFormat(formatFechaSqlite);
            convertDate = formatoIniFecha.parse(valor);
            SimpleDateFormat formatoFinFecha = new SimpleDateFormat(formatFecha);
            aux = formatoFinFecha.format(convertDate);
        } catch (ParseException e) {
            aux = "";
        }
        return aux;
    }

    public static Date formatFecha(String valor, boolean origenSqlite) {
        Date convertDate = new Date();
        SimpleDateFormat formatoIniFecha;

        if (origenSqlite)
            formatoIniFecha = new SimpleDateFormat(formatFechaSqlite);
        else
            formatoIniFecha = new SimpleDateFormat(formatFecha);

        try {
            convertDate = formatoIniFecha.parse(valor);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return convertDate;
    }

    public static long fechaDiff(Date f1, Date f2) {
        return (f1.getTime() - f2.getTime()) / 86400000;
    }

    public static String sqliteFecha(Date fecha) {
        SimpleDateFormat formatoDeFecha = new SimpleDateFormat(formatFechaSqlite);
        return formatoDeFecha.format(fecha);
    }

    public static String sqliteFecha(String dateString) throws java.text.ParseException {
        String aux = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat(formatFecha);
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dateString);
            SimpleDateFormat formatoDeFecha = new SimpleDateFormat(formatFechaSqlite);
            aux = formatoDeFecha.format(convertedDate);
        } catch (ParseException e) {
            aux = null;
        }

        return aux;
    }

    public static String sqliteNumero(double d) {
        return formatUnids.format(d).replace(",", ".");
    }

    public static String formatearImporte(double d) {
        d = Math.rint(d * 100) / 100;
        return formatEuro.format(d);
    }

    public static String formatearUnidades(double d) {
        return formatUnids.format(d);
    }

    public static String formatearUnidades(double d, boolean conSigno) {
        return formatUnidsSgn.format(d);
    }

    public static String LlenaCeros(String d) {
        if (!d.matches("[0-9]+"))
            return (d + "             ").substring(0, 13);
        else
            return ("0000000000000").substring(0, 13 - d.length()) + d;
    }

    public static void sonidazo(boolean soloVibrar) {

        try {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(
                    getContext().getApplicationContext());

            if (soloVibrar)
                builder.setDefaults(Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS);
            else
                builder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE
                        | Notification.DEFAULT_LIGHTS);

            NotificationManager manager = (NotificationManager) getContext()
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            manager.notify(1, builder.build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static boolean miPlaySound(double duration, int freqOfTone) {
        int sampleRate = 8000; // valor fijo

        double dnumSamples = (duration / 1000) * sampleRate;
        dnumSamples = Math.ceil(dnumSamples);
        int numSamples = (int) dnumSamples;
        double sample[] = new double[numSamples];
        byte generatedSnd[] = new byte[2 * numSamples];


        for (int i = 0; i < numSamples; ++i) {
            sample[i] = Math.sin(freqOfTone * 2 * Math.PI * i / (sampleRate));
        }

        int idx = 0;

        for (double dVal : sample) {
            short val = (short) ((dVal * 32767));
            generatedSnd[idx++] = (byte) (val & 0x00ff);
            generatedSnd[idx++] = (byte) ((val & 0xff00) >>> 8);
        }

        AudioTrack audioTrack = null;
        try {
            audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
                    sampleRate, AudioFormat.CHANNEL_CONFIGURATION_MONO,
                    AudioFormat.ENCODING_PCM_16BIT, generatedSnd.length,
                    AudioTrack.MODE_STATIC);
            audioTrack.write(generatedSnd, 0, generatedSnd.length);
            audioTrack.play();
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    public static void playSound(int duration, int freqOfTone, int nb, double volumen, boolean vibrar, int pausa) {

        getContext();
        AudioManager audioManager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);
        volumen = (volumen * audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC)) / 100;
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, (int) volumen, 0);

        if (vibrar) sonidazo(true);
        for (int i = 1; i <= nb; i++) {
            miPlaySound(duration, freqOfTone);
            try {
                Thread.sleep(duration + pausa);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        try {
            Thread.sleep(duration);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void tono(int tono, int durationMs) {
        ToneGenerator tg = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
        tg.startTone(tono, durationMs);
    }

    public static void mostrarToastSimple(String msg, boolean pufa) {
        try {
            View layout = null;
            TextView text = null;
            // get your custom_toast.xml layout
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (pufa) {
                layout = inflater.inflate(R.layout.mitoast_rojo, null);
                text = (TextView) layout.findViewById(R.id.txToastR);
            } else {
                layout = inflater.inflate(R.layout.mitoast_azul, null);
                text = (TextView) layout.findViewById(R.id.txToastA);
            }
            // set a message

            text.setText(msg);

            // Toast...
            Toast toast = new Toast(getContext());
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setDuration(Toast.LENGTH_LONG);
            toast.setView(layout);
            toast.show();


			/*Toast toast = Toast.makeText(getContext(), msg, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
			TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
			v.setBackgroundColor(Color.WHITE);
			v.setTextColor(Color.BLACK);
			toast.show();*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static ProgressDialog pDialog;

    public static void mostrarProgreso(String msg, boolean show, Context c) {
        if (pDialog == null) pDialog = new ProgressDialog(c);
        if (show) {
            pDialog.setMessage(msg);
            pDialog.setTitle("");
            pDialog.show();
        } else {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    public static void msgBox(String titulo, String subTitulo, Context pContexto) {
        AlertDialog.Builder builder = new AlertDialog.Builder(pContexto);
        builder.setTitle(titulo)
                .setMessage(subTitulo)
                .setIcon(R.mipmap.ic_launcher)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        builder.create().show();
    }


    public static void setContext(Context c) {

        ctx = c;
    }

    public static void pausa(int miliseconds) {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

            }
        }, miliseconds);
    }

    public static void enfoca(EditText et, Activity a) {
        et.clearFocus();
        et.setFocusableInTouchMode(true);
        et.setFocusable(true);
        if (et.requestFocus()) {
            et.setCursorVisible(true);
            a.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public static Context getContext() {
        return ctx;
    }

    public static boolean tengoInternet() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected())
            return true;
        else {
            Opciones.setErr(Opciones.Errores.ERROR_SIN_CONEXION.getValor());
            return false;
        }
    }

    public static String getPackageName() {
        return ctx.getPackageName();
    }

    public static String getAppName() {
        final PackageManager pm = ctx.getPackageManager();
        ApplicationInfo ai;
        try {
            ai = pm.getApplicationInfo(ctx.getPackageName(), 0);
        } catch (final NameNotFoundException e) {
            ai = null;
        }
        return (String) (ai != null ? pm.getApplicationLabel(ai) : "(unknown)");
    }

    public static boolean getPocaMemoria() {
        ActivityManager activityManager = (ActivityManager) getContext().getSystemService(ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        activityManager.getMemoryInfo(memoryInfo);
        return memoryInfo.lowMemory;
    }

    public static boolean getTengoPermiso(String permiso, Context ctx) {
        boolean ok = true;

        try {
            if (ContextCompat.checkSelfPermission(ctx, permiso) != PackageManager.PERMISSION_GRANTED) {
                ok = false;
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) ctx, permiso)) {
                    ok = true;
                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions((Activity) ctx, new String[]{permiso}, 1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            ok = false;
        }
        if (!ok) Opciones.setErr(Opciones.Errores.ERROR_SIN_PERMISO.getValor());
        return ok;
    }

    public static boolean mostrarError(Context ctx) {
        if (!Opciones.getErr().equals(Opciones.Errores.ERROR_TODO_OK)) {
            Util.mostrarToastSimple(Opciones.getErr().getDescripcion(), true);
            return false;
        }
        return true;
    }

    public static void abrirPdf(String myFilePath) {
        File fileWithinMyDir = new File(myFilePath);
        Uri path = Uri.fromFile(fileWithinMyDir);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(path, "application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getContext().startActivity(intent);


        /*Intent intentShareFile = new Intent(Intent.ACTION_SEND);
        File fileWithinMyDir = new File(myFilePath);

        if(fileWithinMyDir.exists()) {
            intentShareFile.setType("application/pdf");
            intentShareFile.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:/"+myFilePath));

            intentShareFile.putExtra(Intent.EXTRA_SUBJECT,
                    "Compartiendo PDF...");
            intentShareFile.putExtra(Intent.EXTRA_TEXT, "Compartiendo PDF...");
            intentShareFile.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            getContext().startActivity(Intent.createChooser(intentShareFile, "PDF compartido"));
        }*/
    }

    public static void verDetalle(Maestro Doc, Context ctx) {
        Doc.leerDoc(true);
        String subtit = "Fecha " + formatearFecha(Doc.getFecha()) + "\n";
        switch (Doc.getTipo().getResumenDoc()) {
            case VENTAS:
                subtit = subtit +
                        "Almacén: " + Doc.getNomAlmOrigen() + "\n" +
                        "Cliente: " + Doc.getNomCliente() + "\n";
                break;
            case COMPRAS:
                subtit = subtit +
                        "Almacén: " + Doc.getNomAlmOrigen() + "\n" +
                        "Proveedor: " + Doc.getNomCliente() + "\n";
                break;
            default:
                if (Doc.getTipo() == TRASPASO)
                    subtit = subtit +
                            "Origen: " + Doc.getNomAlmOrigen() + "\n" +
                            "Destino: " + Doc.getNomAlmDestino() + "\n";
                else
                    subtit = subtit +
                            "Almacén: " + Doc.getNomAlmOrigen() + "\n";

        }

        subtit = subtit +
                "Número de artículos: " + Doc.getNumArticulos() + "\n" +
                "Total unidades: " + Doc.getTotalUnidades() + "\n" +
                "Observación: " + Doc.getObs() + "\n" +
                ((Doc.getTocado() == 0) ? "Sincronizado con servidor" : "Pendiente de sincronizar");


        Util.msgBox(Doc.getTipo().getNombre() + " " + Doc.getNumero(), subtit, ctx);
    }

    public static android.support.v7.app.AlertDialog eliminaDoc(final View view, final ArrayList<ItemDocs> items, final int position, final RecyclerView.Adapter adapter, final boolean delAll) {
        String msg;
        final ItemDocs it = items.get(position);
        final Maestro master = new Maestro(it.getTipo(), it.getNumero(), null);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(view.getContext());

        if (!delAll) {
            builder.setTitle("¿Confirma eliminar?");
            msg = "Se eliminará:\n" +
                    it.getTipo().getNombre() + " del día " + it.getFecha() +
                    " que contiene " + master.getNumLineas() + " registros";
        } else {
            builder.setTitle("¿Confirma eliminar TODOS los documentos?");
            msg = "Se eliminará " + items.size() + " documentos.";
        }

        builder.setMessage(msg);

        builder.setPositiveButton("eliminar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (delAll) {
                            if (master.eliminarTodo()) {
                                items.clear();
                                if (adapter != null) adapter.notifyDataSetChanged();
                                Util.msgBox("Documentos eliminados", "TODOS los documentos eliminados correctamente.", view.getContext());
                            }
                        } else {
                            if (master.eliminar()) {
                                items.remove(position);
                                if (adapter != null) adapter.notifyDataSetChanged();
                                Util.msgBox("Registro eliminado", it.getTipo().getNombre() + " eliminado correctamente.", view.getContext());
                            }
                        }
                    }
                }
        );

        builder.setNegativeButton("Cancelar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }
        );

        return builder.create();
    }

    public static String getVersion() {
        try {
            return getContext().getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
            return "";
        }

    }

    public static String getFechaVersion(){
        try
        {
            long installed = getContext().getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_META_DATA).firstInstallTime;
            SimpleDateFormat formatoDeFecha = new SimpleDateFormat(formatFecha, Locale.getDefault());
            return formatoDeFecha.format(installed);
        }
        catch (NameNotFoundException e)
        {
            e.printStackTrace();
            return "";
        }
    }

    public static String getStringFile(File f) {
        InputStream inputStream;
        String encodedFile= "", lastVal;
        try {
            inputStream = new FileInputStream(f.getAbsolutePath());

            byte[] buffer = new byte[10240];//specify the size to allow
            int bytesRead;
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            Base64OutputStream output64 = new Base64OutputStream(output, Base64.DEFAULT);

            while ((bytesRead = inputStream.read(buffer)) != -1) {
                output64.write(buffer, 0, bytesRead);
            }
            output64.close();
            encodedFile =  output.toString();
        }
        catch (FileNotFoundException e1 ) {
            e1.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        lastVal = encodedFile;
        return lastVal;
    }

    private static boolean LOGGING_ENABLED;

    private static final int STACK_TRACE_LEVELS_UP = 5;

    public static void miError(String tag, String message) {
        if (LOGGING_ENABLED) {
            Log.v(tag, getClassNameMethodNameAndLineNumber() + message);
        }
    }

    private static int getLineNumber() {
        return Thread.currentThread().getStackTrace()[STACK_TRACE_LEVELS_UP].getLineNumber();
    }

    private static String getClassName() {
        String fileName = Thread.currentThread().getStackTrace()[STACK_TRACE_LEVELS_UP].getFileName();
        return fileName.substring(0, fileName.length() - 5);
    }

    private static String getMethodName() {
        return Thread.currentThread().getStackTrace()[STACK_TRACE_LEVELS_UP].getMethodName();
    }

    private static String getClassNameMethodNameAndLineNumber() {
        return "[" + getClassName() + "." + getMethodName() + "()-" + getLineNumber() + "]: ";
    }


}
