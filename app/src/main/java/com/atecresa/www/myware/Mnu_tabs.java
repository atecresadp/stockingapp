package com.atecresa.www.myware;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import SqLite.Cnx;
import facturacion.AdapterDocs;
import facturacion.ItemDocs;
import facturacion.MiDoc;

import static com.atecresa.www.myware.Opciones.TipoDoc.ALB_VENTA;
import static com.atecresa.www.myware.Opciones.TipoDoc.FRA_VENTA;
import static com.atecresa.www.myware.Opciones.TipoDoc.INCIDENCIA;
import static com.atecresa.www.myware.R.id.container;
import static com.atecresa.www.myware.Util.grupo_Activo;

public class Mnu_tabs extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mnu__tabs);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setSubtitle(R.string.lb_docsAsoc);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        final TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent inte = new Intent();
                int a = tabLayout.getSelectedTabPosition() + 1;
                inte.putExtra("IdDoc", asignaId(a));
                inte.putExtra("Numero", -1);
                inte.setClass(Mnu_tabs.this, MiDoc.class);
                startActivity(inte);
                finish();
            }
        });
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_settings);
        item.setVisible(false);
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mnu__tabs, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private static Opciones.TipoDoc asignaId(int pos) {
        switch (grupo_Activo) {
            case ALMACEN:
                switch (pos) {
                    case 1:
                        return Opciones.TipoDoc.REG_INVENTA;
                    case 2:
                        return Opciones.TipoDoc.TRASPASO;
                    case 3:
                        return INCIDENCIA;
                }
            case VENTAS:
                switch (pos) {
                    case 1:
                        return FRA_VENTA;
                    case 2:
                        return ALB_VENTA;
                    case 3:
                        return Opciones.TipoDoc.PED_VENTA;
                    case 4:
                        return Opciones.TipoDoc.PRESUPUESTO;
                }
            default:
                switch (pos) {
                    case 1:
                        return Opciones.TipoDoc.FRA_COMPRA;
                    case 2:
                        return Opciones.TipoDoc.ALB_COMPRA;
                    case 3:
                        return Opciones.TipoDoc.PED_COMPRA;
                }
        }
        return null;
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_TIPO_DOC = "tipo_doc";
        private static final String ARG_TIPO_CLI = "tipo_cli";

        public PlaceholderFragment() {
        }

        RecyclerView recVLista;
        AdapterDocs adapter = null;
        RecyclerView.LayoutManager layoutMg;
        ArrayList<ItemDocs> items;

        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            String tipoCli = "C";
            if (grupo_Activo == Opciones.GrupoDocs.COMPRAS) tipoCli = "P";

            args.putInt(ARG_TIPO_DOC, asignaId(sectionNumber).getValor());
            args.putString(ARG_TIPO_CLI, tipoCli);

            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.fragment_mnu__tabs, container, false);


            items = new ArrayList<>();
            recVLista = (RecyclerView) rootView.findViewById(R.id.section_lista);
            recVLista.setHasFixedSize(true);
            layoutMg = new LinearLayoutManager(rootView.getContext());
            recVLista.setLayoutManager(layoutMg);
            adapter = new AdapterDocs(items);
            recVLista.setAdapter(adapter);


            Cursor rs = Cnx.exeSQL("SELECT ID, TIPO, TOCADO, NUMERO, FECHA, OBS, " +
                    "(SELECT NOMBRE FROM ALMACENES WHERE ID = MAESTRO.IDALM) ORIGEN, " +
                    "(SELECT NOMBRE FROM ALMACENES WHERE ID = MAESTRO.IDALMDEST) DESTINO, " +
                    "(SELECT NOMBRE FROM CLIPROV WHERE ID = MAESTRO.IDCLIPROV AND TIPO = '" +
                    getArguments().getString(ARG_TIPO_CLI) + "') CLIENTE " +
                    "FROM MAESTRO WHERE TIPO = " + getArguments().getInt(ARG_TIPO_DOC) + " ORDER BY FECHA DESC");

            if (rs.moveToFirst()) {
                do {
                    ItemDocs its = new ItemDocs(
                            Opciones.TipoDoc.getById(rs.getInt(rs.getColumnIndex("TIPO"))),
                            rs.getInt(rs.getColumnIndex("NUMERO")),
                            rs.getString(rs.getColumnIndex("FECHA")),
                            rs.getString(rs.getColumnIndex("ORIGEN")),
                            rs.getString(rs.getColumnIndex("DESTINO")),
                            rs.getString(rs.getColumnIndex("CLIENTE")),
                            rs.getString(rs.getColumnIndex("OBS")),
                            rs.getInt(rs.getColumnIndex("TOCADO")),
                            rs.getInt(rs.getColumnIndex("ID")));
                    items.add(its);
                } while (rs.moveToNext());
            }
            adapter.notifyDataSetChanged();
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            int i = 0;
            for (Opciones.TipoDoc td : Opciones.TipoDoc.values()) {
                if (td.getResumenDoc() == grupo_Activo) i++;
            }
            return i;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            int i = 0;
            for (Opciones.GrupoDocs gd : Opciones.GrupoDocs.values()) {
                if (gd.equals(grupo_Activo)) {
                    Mnu_tabs.this.setTitle(grupo_Activo.getTitulo());
                    for (Opciones.TipoDoc td : Opciones.TipoDoc.values()) {
                        if (td.getResumenDoc() == gd) {
                            if (i == position) return td.getNomAbreviado();
                            i++;
                        }
                    }
                }
            }
            return null;
        }

    }
}
