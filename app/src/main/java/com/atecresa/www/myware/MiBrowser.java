package com.atecresa.www.myware;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.MenuItemCompat.OnActionExpandListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import SqLite.Cnx;

/**
 * Created by Gustavo on 16/12/2016.
 */

public class MiBrowser extends AppCompatActivity implements OnActionExpandListener, OnQueryTextListener {
    private String qSql;
    private String titulo;
    private String criterio;
    private Buscar b;
    private ArrayList<String> itWh;

    RecyclerView recVLista;
    RecyclerView.LayoutManager layoutMg;
    ArrayList<ItemBrw> Items;

    AdapterBrw adapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser);

        recVLista = (RecyclerView) findViewById(R.id.brw_recycler);
        recVLista.setHasFixedSize(true);

        layoutMg = new LinearLayoutManager(this);
        recVLista.setLayoutManager(layoutMg);

        adapter = new AdapterBrw(Items);
        recVLista.setAdapter(adapter);

        try {
            Intent intent = getIntent();
            qSql = getIntent().getExtras().getString("SQLbusca");
            titulo = getIntent().getExtras().getString("TituloBrw");
            criterio = getIntent().getExtras().getString("Criterio");
        } catch (Exception ex) {
            finish();
        }

        Items = new ArrayList<ItemBrw>();

        final RecyclerView rv = (RecyclerView) findViewById(R.id.brw_recycler);
        adapter = new AdapterBrw(Items);
        rv.setAdapter(adapter);

        setupActionBar();
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_browser, menu);
        MenuItem searchItem = menu.findItem(R.id.app_bar_search);

        SearchManager searchManager = (SearchManager) MiBrowser.this.getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = null;

        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
            searchItem.expandActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(MiBrowser.this.getComponentName()));
        }
        if (searchView != null) {
            searchView.onActionViewExpanded();
        }
        searchView.setQueryHint(titulo);
        searchView.setOnQueryTextListener(this);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onResume() {
        new Buscar("").execute();
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                setResult(Activity.RESULT_CANCELED);
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextChange(final String s) {
        b = new Buscar(s);
        b.execute();
        return false;
    }


    @Override
    public boolean onQueryTextSubmit(String s) {
        b = new Buscar(s);
        b.execute();
        return false;
    }

    private void consultar(String tx) {
        String wh = "";
        Items.clear();

        if (itWh != null) {
            wh = " WHERE (";
            if (criterio != null) {
                if (!criterio.equals("")) wh = wh + criterio + ") AND (";
            }
            for (int i = 0; i < itWh.size(); i++) {
                wh = wh + itWh.get(i) + " LIKE '%" + tx + "%' OR ";
            }
            wh = wh.substring(0, wh.length() - 4) + ")";
        } else if (criterio != null) {
            wh = " WHERE " + criterio;
        }
        Cursor rs = Cnx.exeSQL(qSql + wh + " LIMIT 300");

        try {
            if (rs.moveToFirst()) {
                if (itWh == null) {
                    itWh = new ArrayList<>();
                    for (int i = 1; i < rs.getColumnCount(); i++)
                        itWh.add(rs.getColumnName(i));
                }
                do {
                    ItemBrw it = new ItemBrw(rs.getInt(0), rs.getString(1), rs.getString(2));
                    Items.add(it);
                } while (rs.moveToNext());

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                rs.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public boolean onMenuItemActionExpand(MenuItem item) {
        return false;
    }

    @Override
    public boolean onMenuItemActionCollapse(MenuItem item) {
        return false;
    }

    private class Buscar extends AsyncTask<Void, Void, Boolean> {

        String aBuscar;

        Buscar(String tx) {
            aBuscar = tx;
            Util.mostrarProgreso(getString(R.string.lb_buscando), true, MiBrowser.this);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            consultar(aBuscar);
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            if (success) {
                adapter.notifyDataSetChanged();
                Util.mostrarProgreso("", false, MiBrowser.this);
            }
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, @NonNull KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            setResult(Activity.RESULT_CANCELED);
            finish();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    public class ItemBrw {
        private int itId;
        private String it1;
        private String it2;

        ItemBrw(int pitid, String pit1, String pit2) {
            it1 = pit1;
            it2 = pit2;
            itId = pitid;
        }

        String getIt1() {
            return it1;
        }

        String getIt2() {
            return it2;
        }

        int getItId() {
            return itId;
        }
    }

    class AdapterBrw extends RecyclerView.Adapter<AdapterBrw.ViewHolder> {

        private ArrayList<ItemBrw> items;

        AdapterBrw(ArrayList<ItemBrw> items) {
            this.items = items;
        }

        @Override
        public AdapterBrw.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_browser, parent, false);
            // set the view's size, margins, paddings and layout parameters

            return new AdapterBrw.ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(AdapterBrw.ViewHolder holder, int position) {
            holder.mItId.setTag(items.get(position).getItId());
            holder.mIt1.setText(items.get(position).getIt1());
            holder.mIt2.setText(items.get(position).getIt2());
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
            TextView mIt1, mIt2;
            LinearLayout mItId;

            ViewHolder(View v) {
                super(v);
                mItId = (LinearLayout) v.findViewById(R.id.itId);
                mIt1 = (TextView) v.findViewById(R.id.it1);
                mIt2 = (TextView) v.findViewById(R.id.it2);
                LinearLayout b = (LinearLayout) v.findViewById(R.id.itId);
                b.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                Intent inte = new Intent(getApplicationContext(), MiBrowser.class);
                inte.putExtra("itemSel", v.findViewById(R.id.itId).getTag().toString());
                setResult(RESULT_OK, inte);
                finish();
            }

            @Override
            public boolean onLongClick(View v) {
                return false;
            }
        }


    }

}