package com.atecresa.www.myware;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Informa extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informa);

        TextView tx = (TextView) findViewById(R.id.id_version);
        tx.setText("v. " + Util.getVersion() + " instalado el " + Util.getFechaVersion());

        Button bt = (Button) findViewById(R.id.id_volver);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Informa.this.finish();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        });
    }
}
