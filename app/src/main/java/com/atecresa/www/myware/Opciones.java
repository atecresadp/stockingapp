package com.atecresa.www.myware;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;

import java.util.Date;

import SqLite.Cnx;

import static com.atecresa.www.myware.Util.getContext;

/**
 * Created by Gustavo on 21/12/2016.
 */

public final class Opciones {

    private static String getGeneral(String nombrePref, String xDef) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        return sharedPref.getString(nombrePref, xDef);
    }

    private static boolean getGeneral(String nombrePref, boolean xDef) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        return sharedPref.getBoolean(nombrePref, xDef);
    }

    private static int getGeneral(String nombrePref, int xDef) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        return sharedPref.getInt(nombrePref, xDef);
    }

    private static void setGeneral(String nombrePref, String valor) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(nombrePref, valor);
        editor.commit();
    }

    private static void setGeneral(String nombrePref, int valor) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(nombrePref, valor);
        editor.commit();
    }

    private static void setGeneral(String nombrePref, boolean valor) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(nombrePref, valor);
        editor.commit();
    }

    private static void setPref(String nomPref, String valor) {
        SharedPreferences settings = getContext().getSharedPreferences("PRIVADA", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(nomPref, valor);
        editor.commit();
    }

    private static void setPref(String nomPref, int valor) {
        SharedPreferences settings = getContext().getSharedPreferences("PRIVADA", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(nomPref, valor);
        editor.commit();
    }

    private static void setPref(String nomPref, boolean valor) {
        SharedPreferences settings = getContext().getSharedPreferences("PRIVADA", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(nomPref, valor);
        editor.commit();
    }

    private static void setPref(String nomPref, Date valor) {
        SharedPreferences settings = getContext().getSharedPreferences("PRIVADA", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong(nomPref, valor.getTime());
        editor.commit();
    }

    private static String getPref(String nomPref, String xDef) {
        SharedPreferences settings = getContext().getSharedPreferences("PRIVADA", 0);
        return settings.getString(nomPref, xDef);
    }

    private static int getPref(String nomPref, int xDef) {
        SharedPreferences settings = getContext().getSharedPreferences("PRIVADA", 0);
        return settings.getInt(nomPref, xDef);
    }

    private static long getPref(String nomPref, long xDef) {
        SharedPreferences settings = getContext().getSharedPreferences("PRIVADA", 0);
        return settings.getLong(nomPref, xDef);
    }

    private static boolean getPref(String nomPref, boolean xDef) {
        SharedPreferences settings = getContext().getSharedPreferences("PRIVADA", 0);
        return settings.getBoolean(nomPref, xDef);
    }

    private static Date getPref(String nomPref, Date xDef) {
        SharedPreferences settings = getContext().getSharedPreferences("PRIVADA", 0);
        Date mydate = new Date(settings.getLong(nomPref, xDef.getTime()));
        return mydate;
    }

    // Preferencias de los settings

    public static TipoDoc getIdDocXDef() {
        return TipoDoc.getById(getGeneral("Opc_DocXDef", 5));
    }

    public static void SetIdDocXDef(TipoDoc valor) {
        setGeneral("Opc_DocXDef", valor.getValor());
    }

    public static boolean getSincronizaON() { return getGeneral("SincronizaOn", false); };

    public static void setSincronizaON(boolean valor) {
        setGeneral("SincronizaOn", valor);
    }

    public static int getIdAlmacen() {
        return Integer.parseInt(getGeneral("Opc_Almacen", "1"));
    }

    public static String getNombreEmpresa() {
        return getGeneral("Opc_NomEmp", "Empresa Demo");
    }

    public static void setNombreEmpresa(String valor) {
        setGeneral("Opc_NomEmp", valor);
    }

    public static String getPwdAcceso() {
        return getGeneral("Opc_Pwd", "atecresa");
    } //TODO: No está desarrollado

    public static String getSufijo() {
        return getGeneral("Opc_Sufijo", "");
    }

    public static String getHost() {
        return getGeneral("Opc_Host", "ategestservidores.net");
    }

    public static boolean getAvisoArtInex() {
        return getGeneral("Opc_AvisoArt", true);
    }

    public static boolean getDevolObs() {
        return getGeneral("Opc_DevolObs", true);
    } //TODO: No está desarrollado

    public static boolean getLecturaAuto() {
        return getGeneral("Opc_LecturaAuto", false);
    }

    public static int getTamCod() {
        return Integer.parseInt(getGeneral("Opc_TamCod", "13"));
    }

    public static boolean getCrearArt() {
        return getGeneral("Opc_CrearArt", false);
    }

    public static boolean getCamaraOn() {
        return getGeneral("Opc_CamOn", false);
    }


    // Opciones internas

    //public static final String getUrlServidor = "http://ategestservidores.net/AtegestServ/ServiciosJsonNoDecode?";
    //public static final String getUrlServidor = "http://softwaretpvgestion.com/AtegestServ/ServiciosJsonNoDecode?";
    public static String getUrlServidor() {
        return "http://" + Opciones.getHost() + "/AtegestServ/ServiciosJsonStream?";
    }

    public static String urlServidorPdf() {
        return "http://" + Opciones.getHost() + "/AtegestServ/ServiciosDescarga?";
    }

    public static int getIdUsuario() {
        return getPref("Opc_IdUsuario", 1);
    }

    public static void setIdUsuario(int valor) {
        setPref("Opc_IdUsuario", valor);
    }

    public static String getEmailUsuario() {
        return getPref("Opc_EmailUsuario", "");
    }

    public static void setEmailUsuario(String valor) {
        setPref("Opc_EmailUsuario", valor);
    }

    public static String getCodigoUsuario() {
        return getPref("Opc_CodigoUsuario", "");
    }

    public static void setCodigoUsuario(String valor) {
        setPref("Opc_CodigoUsuario", valor);
    }

    public static String getNombreUsuario() {
        return getPref("Opc_NombreUsuario", "");
    }

    public static void setNombreUsuario(String valor) {
        setPref("Opc_NombreUsuario", valor);
    }

    public static String getPasswordUsuario() {
        return getPref("Opc_PasswordUsuario", "");
    }

    public static void setPasswordUsuario(String valor) {
        setPref("Opc_PasswordUsuario", valor);
    }

    public static String getSerial() {
        if (Util.getTengoPermiso(android.Manifest.permission.READ_PHONE_STATE, getContext())) {
            TelephonyManager tManager = (TelephonyManager) getContext().getSystemService(Context.TELEPHONY_SERVICE);
            return tManager.getDeviceId();
        } else return "";
    }

    public static String getUuid() {
        return getPref("Opc_Uuid", "NULL");
    }

    public static void setUuid(String valor) {
        setPref("Opc_Uuid", valor);
    }

    public static String getIdDispositivo() {
        return Secure.getString(getContext().getContentResolver(), Secure.ANDROID_ID);
    }

    public static String getCarpetaDown() {
        return String.valueOf(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS));
    }

    public static String getNomAlmacen() {
        return Cnx.Buscar("SELECT NOMBRE FROM ALMACENES WHERE ID = " + getIdAlmacen(), "");
    }

    public static String getIdTerminal() {
        return getPref("Opc_IdTerminal", "");
    }

    public static void setIdTerminal(String valor) {
        setPref("Opc_IdTerminal", valor);
    }

    public static String getNomTerminal() {
        return getPref("Opc_NomTerminal", "");
    }

    public static void setNomTerminal(String valor) {
        setPref("Opc_NomTerminal", valor);
    }

    //region Gestión de Licencia

    public static String getLicencia() {
        return getPref("Opc_Licencia", "");
    }

    public static void setLicencia(String valor) {
        setPref("Opc_Licencia", valor);
    }

    public static Date getCaducidad() {
        return getPref("Opc_Caduca", Util.formatFecha("01/01/2050 00:00:00", false));
    }

    public static void setCaducidad(Date valor) {
        setPref("Opc_Caduca", valor);
    }

    public static Errores getErr() {
        return Errores.getById(getPref("Opc_Err", Errores.ERROR_TODO_OK.getValor()));
    }

    public static void setErr(int valor) {
        setPref("Opc_Err", valor);
    }

    //endregion

    //region Tipos enumerados

    public static enum MostrarX {
        CERRADO(true, R.drawable.ic_arrow_drop_down_black_24dp, Util.getContext().getString(R.string.lb_docsHechos)),
        ABIERTO(false, R.drawable.ic_arrow_drop_up_black_24dp, Util.getContext().getString(R.string.lb_docsHecho));

        boolean valor;
        int icono;
        String titulo;

        MostrarX(boolean i, int ico, String tit) {
            valor = i;
            icono = ico;
            titulo = tit;
        }

        public String getTitulo() {
            return titulo;
        }

        public boolean getValor() {
            return valor;
        }

        public int getIco() {
            return icono;
        }

    }

    public static enum GrupoDocs {

        VENTAS(0, "Ventas", R.string.title_activity_mnu__ventas),
        COMPRAS(1, "Compras", R.string.title_activity_mnu__compras),
        ALMACEN(2, "Almacén", R.string.title_activity_mnu__almacen);

        int valor;
        String nombre;
        int titulo;

        GrupoDocs(int i, String nom, int tit) {
            valor = i;
            nombre = nom;
            titulo = tit;
        }

        public static GrupoDocs getById(int i) {
            for (GrupoDocs gd : GrupoDocs.values()) {
                if (i == gd.getValor()) return gd;
            }
            return null;
        }

        public int getValor() {
            return valor;
        }

        public String getNombre() {
            return nombre;
        }

        public String getTitulo() {
            Resources res = getContext().getResources();
            return res.getString(titulo);
        }
    }

    public static enum TipoDoc {

        FRA_VENTA(1, "Factura Venta", GrupoDocs.VENTAS, "NFV"),
        FRA_COMPRA(2, "Factura Compra", GrupoDocs.COMPRAS, "NFC"),
        ALB_VENTA(3, "Albarán Venta", GrupoDocs.VENTAS, "NAV"),
        ALB_COMPRA(4, "Albarán Compra", GrupoDocs.COMPRAS, "NAC"),
        REG_INVENTA(5, "Regularización Inventario", GrupoDocs.ALMACEN, "NRG"),
        TRASPASO(6, "Traspaso", GrupoDocs.ALMACEN, "NTR"),
        PED_VENTA(8, "Pedido Venta", GrupoDocs.VENTAS, "NPV"),
        PED_COMPRA(9, "Pedido Compra", GrupoDocs.COMPRAS, "NPC"),
        PRODUCCION(19, "Producción", GrupoDocs.ALMACEN, "NPR"),
        PRESUPUESTO(23, "Presupuesto", GrupoDocs.VENTAS, "NPE"),
        INCIDENCIA(20, "Incidencia Stock", GrupoDocs.ALMACEN, "NIS");

        //GASTO(22,"Gasto", "Compras");

        int valor;
        String nombre;
        GrupoDocs resumenDoc;
        String nomCampo;

        TipoDoc(int i, String nom, GrupoDocs resDoc, String nomC) {
            valor = i;
            nombre = nom;
            resumenDoc = resDoc;
            nomCampo = nomC;
        }

        public static TipoDoc getById(int i) {
            for (TipoDoc td : TipoDoc.values()) {
                if (i == td.getValor()) return td;
            }
            return null;
        }

        public int getValor() {
            return valor;
        }

        public String getNombre() {
            return nombre;
        }

        public GrupoDocs getResumenDoc() {
            return resumenDoc;
        }

        public String getSuCliente() {
            if (resumenDoc == GrupoDocs.VENTAS)
                return "Cliente";
            else if (resumenDoc == GrupoDocs.COMPRAS) {
                if (valor == 22)
                    return "Acreedor";
                else
                    return "Proveedor";
            }
            return "";
        }

        public String getTipoCli() {
            return getSuCliente().substring(1, 2);
        }

        public String getNomAbreviado() {
            return nombre.split(" ")[0];
        }

        public int getIcono() {
            switch (resumenDoc) {
                default:
                    if (getValor() != 6)
                        return R.drawable.ic_regularizar;
                    else
                        return R.drawable.ic_traspaso;
                case VENTAS:
                    return R.drawable.ic_euros;
                case COMPRAS:
                    return R.drawable.ic_compras;
            }
        }

        public String getNomCampo() {
            return nomCampo;
        }
    }

    public static enum Errores {

        ERROR_LICENCIA_CADUCA(1, Util.getContext().getString(R.string.lb_err_LicCaduca)),
        ERROR_LICENCIA_PROXIMA_CADUCAR(2, Util.getContext().getString(R.string.lb_err_LicCasiCaduca)),
        ERROR_LICENCIA_FUERA_FECHA(3, Util.getContext().getString(R.string.lb_err_LicFueraFecha)),
        ERROR_LICENCIA_INACTIVA(4, Util.getContext().getString(R.string.lb_err_LicInactiva)),
        ERROR_LICENCIA_NO_PERTENECE(5, Util.getContext().getString(R.string.lb_err_LicNoEs)),
        ERROR_TEST_FAIL(6, Util.getContext().getString(R.string.lb_err_testMalo)),
        ERROR_LOGIN_FAIL(7, Util.getContext().getString(R.string.lb_err_LoginFallo)),
        ERROR_SIN_CONEXION(8, Util.getContext().getString(R.string.lb_err_SinCnx)),
        ERROR_FALLO_CONEXION(9, Util.getContext().getString(R.string.lb_err_FalloCnx)),
        ERROR_SIN_PERMISO(10, Util.getContext().getString(R.string.lb_err_SinPermiso)),
        ERROR_DEMASIADOS_INTENTOS(11, "Se han hecho demasiados intentos con este usuario y contraseña. Inténtelo de nuevo en unos minutos."),
        ERROR_TODO_OK(100, "");

        String descripcion;
        int index;

        Errores(int indx, String describe) {
            descripcion = describe;
            index = indx;
        }

        public static Errores getById(int i) {
            for (Errores er : Errores.values()) {
                if (i == er.getValor()) return er;
            }
            return null;
        }

        public String getDescripcion() {
            return descripcion;
        }

        public int getValor() {
            return index;
        }
    }

    //endregion

    //region Las CONSTANTES

    public static class Constantes {
        //public static final String getUrlServidor = "http://192.168.0.6/AtegestServ/ServiciosJsonNoDecode?";
        public static final String Character_Set = "UTF-8";
        public static final int licMiModulo = 290;
        public static final String getIdProyecto = "33391089547";
        public static final String getUrlDescarga = "https://www.atecresa.com/descargas/android/stockingapp/app-release.apk";
    }
    //endregion

}
