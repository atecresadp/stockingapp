package com.atecresa.www.myware;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;

import SqLite.Cnx;
import facturacion.AdapterDocs;
import facturacion.ItemDocs;
import facturacion.MiDoc;

import static com.atecresa.www.myware.Opciones.GrupoDocs.VENTAS;
import static com.atecresa.www.myware.Opciones.MostrarX.ABIERTO;
import static com.atecresa.www.myware.Opciones.MostrarX.CERRADO;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private GoogleApiClient client;
    private Toolbar toolbar;
    private Opciones.MostrarX seMuestra = null;


    RecyclerView recVLista;
    AdapterDocs adapter = null;
    RecyclerView.LayoutManager layoutMg;
    ArrayList<ItemDocs> items;
    TextView porDef = null;
    TextView otro = null;
    FloatingActionButton fab1;
    FloatingActionButton fab2;
    ImageButton btMuestra;
    MenuItem itemComunica;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Cnx.IniciarBD(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setSubtitle(R.string.tit_subtitulo);

        items = new ArrayList<>();
        recVLista = (RecyclerView) findViewById(R.id.id_rvMain);
        recVLista.setHasFixedSize(true);
        layoutMg = new LinearLayoutManager(this);
        recVLista.setLayoutManager(layoutMg);
        adapter = new AdapterDocs(items, this);

        recVLista.setAdapter(adapter);

        Util.setContext(this.getApplicationContext());
        seMuestra = CERRADO;

        porDef = (TextView) findViewById(R.id.id_docxdef);
        otro = (TextView) findViewById(R.id.id_otro);

        btMuestra = (ImageButton) findViewById(R.id.id_muestra);
        btMuestra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView tx = (TextView) findViewById(R.id.id_DocCount);
                if (seMuestra == ABIERTO) {
                    seMuestra = CERRADO;
                    tx.setText(seMuestra.getTitulo());
                } else {
                    seMuestra = ABIERTO;
                    if (tx.getTag() != null)
                        tx.setText((String) tx.getTag());
                    else
                        tx.setText(seMuestra.getTitulo());
                }

                btMuestra.setImageResource(seMuestra.icono);
            }
        });

        fab1 = (FloatingActionButton) findViewById(R.id.fab1);
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent inte = new Intent();
                inte.putExtra("IdDoc", Opciones.getIdDocXDef());
                inte.putExtra("Numero", -1);
                inte.setClass(MainActivity.this, MiDoc.class);
                startActivity(inte);
                muestraBt(false);
            }
        });

        fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CambiaDoc().show();
                muestraBt(false);
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                muestraBt(fab1.getVisibility() == View.INVISIBLE);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            MainActivity.this.startActivity(new Intent(MainActivity.this, SettingsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public AlertDialog CambiaDoc() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String[] docs = new String[Opciones.TipoDoc.values().length];
        final int[] val_docs = new int[Opciones.TipoDoc.values().length];
        int pos = 5;
        int i = 0;

        for (Opciones.TipoDoc td : Opciones.TipoDoc.values()) {
            if (td == Opciones.getIdDocXDef()) pos = i;
            docs[i] = td.getNombre();
            val_docs[i] = td.getValor();
            i++;
        }

        builder.setTitle(R.string.lb_elegirDoc);
        builder.setSingleChoiceItems(docs, pos, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Opciones.SetIdDocXDef(Opciones.TipoDoc.getById(val_docs[which]));
                mostrarResumen();
                muestraBt(true);
            }
        });
        return builder.create();
    }

    private void muestraBt(boolean mostrar) {
        if (mostrar) {
            fab1.setImageResource(Opciones.getIdDocXDef().getIcono());
            porDef.setText(Opciones.getIdDocXDef().getNombre());
            porDef.setTextColor(Color.WHITE);
            porDef.setBackgroundColor(Color.BLACK);
            otro.setTextColor(Color.WHITE);
            otro.setBackgroundColor(Color.BLACK);
        }
        fab1.setVisibility(mostrar ? View.VISIBLE : View.INVISIBLE);
        fab2.setVisibility(mostrar ? View.VISIBLE : View.INVISIBLE);
        porDef.setVisibility(mostrar ? View.VISIBLE : View.INVISIBLE);
        otro.setVisibility(mostrar ? View.VISIBLE : View.INVISIBLE);
    }

    private void abrirDoc(ItemDocs it) {
        Intent inte = new Intent();
        inte.putExtra("IdDoc", it.getTipo());
        inte.putExtra("Numero", it.getNumero());
        inte.setClass(MainActivity.this, MiDoc.class);
        startActivity(inte);
    }

    public int mostrarResumen() {
        int icono = 0;
        TextView tx;

        CardView t = (CardView) findViewById(R.id.tarj1);

        this.setTitle(Opciones.getNombreEmpresa());
        toolbar.setSubtitle(getString(R.string.lb_operador) + Opciones.getNombreUsuario());

        tx = (TextView) findViewById(R.id.id_version);
        tx.setText("v. " + Util.getVersion());

        tx = (TextView) findViewById(R.id.id_almacen);
        String aux = Opciones.getNomAlmacen();
        if (aux.equals("")) tx.setVisibility(View.GONE);
        tx.setText(getString(R.string.lb_almacen) + aux);
        tx = (TextView) findViewById(R.id.id_terminal);
        if (Opciones.getIdTerminal().equals("")) tx.setVisibility(View.INVISIBLE);
        tx.setText(getString(R.string.lb_terminal) + Opciones.getIdTerminal() + ", " + Opciones.getNomTerminal());

        StringBuilder lista = new StringBuilder();
        Cursor rs = null;
        try {
            rs = Cnx.exeSQL("SELECT TIPO, COUNT(0) AS CONTA FROM MAESTRO WHERE UUID = '" + Opciones.getUuid() + "' GROUP BY MAESTRO.TIPO");
            if (rs.moveToFirst()) {
                do {
                    lista.append(String.format("%1$" + 3 + "s", rs.getString(rs.getColumnIndex("CONTA")))).append(" ")
                            .append(Opciones.TipoDoc.getById(rs.getInt(rs.getColumnIndex("TIPO"))).getNombre()).append("\n");
                } while (rs.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("MostrarResumen", e.getMessage());
        } finally {
            if (rs != null) rs.close();
        }

        if (lista.length() == 0) {
            t.setVisibility(View.GONE);
            lista.append(getString(R.string.lb_resumen_nomov));
        } else {
            t.setVisibility(View.VISIBLE);
            lista.insert(0, getString(R.string.lb_resumen_movsi));
        }

        tx = (TextView) findViewById(R.id.id_DocCount);
        tx.setTag(lista.toString());
        if (seMuestra == ABIERTO) tx.setText(lista.toString());
        adapter.clear();
        items.clear();
        try {
            rs = Cnx.exeSQL("SELECT ID, TIPO, NUMERO, FECHA, OBS, TOCADO, " +
                    "(SELECT NOMBRE FROM ALMACENES WHERE ID = MAESTRO.IDALM) ORIGEN, " +
                    "(SELECT NOMBRE FROM ALMACENES WHERE ID = MAESTRO.IDALMDEST) DESTINO, " +
                    "(SELECT NOMBRE FROM CLIPROV WHERE ID = MAESTRO.IDCLIPROV) CLIENTE " +
                    "FROM MAESTRO WHERE UUID = '" + Opciones.getUuid() + "' ORDER BY FECHA DESC");
            if (rs.moveToFirst()) {
                do {
                    ItemDocs mit = new ItemDocs(
                            Opciones.TipoDoc.getById(rs.getInt(rs.getColumnIndex("TIPO"))),
                            rs.getInt(rs.getColumnIndex("NUMERO")),
                            rs.getString(rs.getColumnIndex("FECHA")),
                            rs.getString(rs.getColumnIndex("ORIGEN")),
                            rs.getString(rs.getColumnIndex("DESTINO")),
                            rs.getString(rs.getColumnIndex("CLIENTE")),
                            rs.getString(rs.getColumnIndex("OBS")),
                            rs.getInt(rs.getColumnIndex("TOCADO")),
                            rs.getInt(rs.getColumnIndex("ID")));
                    items.add(mit);
                } while (rs.moveToNext());
                adapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) rs.close();
        }
        return 0;
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.it_Almacen) {
            Util.grupo_Activo = Opciones.GrupoDocs.ALMACEN;
            MainActivity.this.startActivity(new Intent(MainActivity.this, Mnu_tabs.class));
        } else if (id == R.id.it_Ventas) {
            Util.grupo_Activo = VENTAS;
            MainActivity.this.startActivity(new Intent(MainActivity.this, Mnu_tabs.class));
        } else if (id == R.id.it_Compras) {
            Util.grupo_Activo = Opciones.GrupoDocs.COMPRAS;
            MainActivity.this.startActivity(new Intent(MainActivity.this, Mnu_tabs.class));
        } else if (id == R.id.it_Comunica) {
            itemComunica = item;
            trasiego();
        } else if (id == R.id.it_Operador) {
            MainActivity.this.startActivity(new Intent(MainActivity.this, LoginActivity.class));
        } else if (id == R.id.it_AcercaDe) {
            MainActivity.this.startActivity(new Intent(MainActivity.this, Informa.class));
        }


        item.setChecked(true);
        //getSupportActionBar().setTitle(item.getTitle());

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Main Page")
                .setUrl(Uri.parse("http://www.atecresa.com"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    protected void onResume() {
        //TODO ¡OJO CON ESTO!
        super.onResume();
        mostrarResumen();
        if (Opciones.getErr().equals(Opciones.Errores.ERROR_LICENCIA_PROXIMA_CADUCAR))
            Util.msgBox(getString(R.string.lb_cadproximo), Opciones.Errores.ERROR_LICENCIA_PROXIMA_CADUCAR.getDescripcion(), this);
        if (Opciones.getSincronizaON()) trasiego();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Util.setContext(this);
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
        mostrarResumen();
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    @Override
    protected void onDestroy() {
        Cnx.Desconecta();
        super.onDestroy();
    }


    private void trasiego() {
        Opciones.setSincronizaON(false);
        Comunica com = new Comunica();
        com.execute((Void) null);
    }

    public class Comunica extends AsyncTask<Void, String, Boolean> {

        Comunica() {
            if (itemComunica!=null){
                itemComunica.setEnabled(false);
                itemComunica.setTitle(R.string.lb_comunica_syncking);
            }
            Util.mostrarProgreso(getString(R.string.lb_comunica_synckings), true, MainActivity.this);
        }

        Nube miNube = new Nube();

        @Override
        protected Boolean doInBackground(Void... params) {
            publishProgress(getString(R.string.lb_comunica_cnx));
            miNube.callIt(Nube.CallWebServ.CALL_TESTCNX);
            if (!Util.mostrarError(MainActivity.this)) return false;
            publishProgress(getString(R.string.lb_comunica_conta));
            miNube.callIt(Nube.CallWebServ.CALL_CONTADORES);
            if (!Util.mostrarError(MainActivity.this)) return false;
            publishProgress(getString(R.string.lb_comunica_envia_art));
            miNube.subirTabla("ARTICULOS");
            if (!Util.mostrarError(MainActivity.this)) return false;
            for (Nube.CallWebServ llama : Nube.CallWebServ.values()) {
                if (llama.funcion.equals("GETTABLA")) {
                    publishProgress(String.format("%s%s", getString(R.string.lb_comunica_recib), llama.getAlias()));
                    miNube.callIt(llama);
                    if (!Util.mostrarError(MainActivity.this)) return false;
                }
            }
            publishProgress(getString(R.string.lb_comunica_env_doc));
            miNube.subirDocs(-1);
            if (!Util.mostrarError(MainActivity.this)) return false;
            return true;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            Util.mostrarProgreso(values[0], true, MainActivity.this);
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            if (itemComunica!=null) {
                itemComunica.setEnabled(true);
                itemComunica.setTitle(R.string.lb_comunica_sync);
            }
            Util.mostrarProgreso("", false, MainActivity.this);
            if (!success) {
                if (Opciones.getErr().equals(Opciones.Errores.ERROR_TEST_FAIL))
                    MainActivity.this.startActivity(new Intent(MainActivity.this, LoginActivity.class));
                else
                    Util.msgBox(getString(R.string.lb_conexion_error), Opciones.getErr().getDescripcion(), MainActivity.this);
            } else
                Util.msgBox(getString(R.string.lb_comunica_syncs), getString(R.string.lb_comunica_result) + miNube.getSucesos(), MainActivity.this);

            mostrarResumen();
        }

        @Override
        protected void onCancelled() {
            itemComunica.setEnabled(true);
            MenuItem menuItem = itemComunica.setTitle(getString(R.string.lb_comunica_sync));
            Util.mostrarProgreso("", false, MainActivity.this);
        }
    }
}
