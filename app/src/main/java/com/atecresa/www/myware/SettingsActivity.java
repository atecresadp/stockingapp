package com.atecresa.www.myware;

//TODO Hay que reiniciar (dispose) todas las activitis después de programar.

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;

import SqLite.Cnx;
import SqLite.MyDatabaseTools;

public class SettingsActivity extends AppCompatPreferenceActivity {

    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */

    SharedPreferences sp;

    private Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {

        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {
            String stringValue = value.toString();

            if (preference instanceof ListPreference) {
                ListPreference listPreference = (ListPreference) preference;
                int index = listPreference.findIndexOfValue(stringValue);

                preference.setSummary(
                        index >= 0
                                ? listPreference.getEntries()[index]
                                : null);

            } else {
                preference.setSummary(stringValue);
            }
            return true;
        }
    };

    private boolean isXLargeTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    private void bindPreferenceSummaryToValue(Preference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        // Trigger the listener immediately with the preference's
        // current value.
        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getString(preference.getKey(), ""));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_documento);

        Cursor rs = Cnx.exeSQL("SELECT CODIGO, NOMBRE FROM ALMACENES");
        StringBuilder nombres = new StringBuilder();
        StringBuilder valores = new StringBuilder();

        if (rs.moveToFirst()) {
            do {
                nombres.append(rs.getString(rs.getColumnIndex("NOMBRE")) + "~~");
                valores.append(rs.getString(rs.getColumnIndex("CODIGO")) + "~~");
            } while (rs.moveToNext());
        }
        if (nombres.length() > 0) {
            ListPreference lp = (ListPreference) findPreference("Opc_Almacen");
            lp.setEntries(nombres.toString().split("~~"));
            lp.setEntryValues(valores.toString().split("~~"));
        }

        Preference button = (Preference) findPreference("Opc_update");
        button.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Descargar com = new Descargar(SettingsActivity.this);
                com.execute((Void) null);
                return true;
            }
        });

        Preference button1 = (Preference) findPreference("Opc_Backup");
        button1.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                try {
                    Backup bk = new Backup();
                    bk.execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            }
        });

        Preference button2 = (Preference) findPreference("Opc_Restore");
        button2.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                try {
                    restauralo();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            }
        });

        bindPreferenceSummaryToValue(findPreference("Opc_Almacen"));
        bindPreferenceSummaryToValue(findPreference("Opc_TamCod"));
        bindPreferenceSummaryToValue(findPreference("Opc_Host"));

        setupActionBar();
    }

    private void restauralo(){
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setTitle("¿Confirma restaurar la base de datos?");
        String msg = "Se eliminará todos los datos de la base de datos actual";
        builder.setMessage(msg);

        builder.setPositiveButton("Restaurar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    new Restore().execute();
                    }
                }
        );

        builder.setNegativeButton("Cancelar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }
        );

        builder.create();
        builder.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    private void setupActionBar() {
        ActionBar actionBar = this.getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    class Descargar extends AsyncTask<Void, String, String> {
        private Nube miNube = new Nube();
        private Context context;

        Descargar(Context ctext) {
            Nube miNube = new Nube();
            context = ctext;
        }

        @Override
        protected String doInBackground(Void... params) {
            publishProgress(getString(R.string.lb_descarga_upd));
            miNube.descargarUpd(context);
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            Util.mostrarProgreso(values[0], true, context);
        }

        @Override
        protected void onPostExecute(final String success) {
            Util.mostrarProgreso("", false, context);
        }

    }

    private class Backup extends AsyncTask<Object, Object, Boolean> {

        @Override
        protected void onPreExecute() {
            Util.mostrarProgreso("Haciendo backup...", true, SettingsActivity.this);
        }

        @Override
        protected Boolean doInBackground(Object... arg0) {
            return MyDatabaseTools.backup();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            Util.mostrarProgreso("", false, null);
            if (!result)
                Util.mostrarToastSimple("No se ha podido hacer la copia de seguridad", true);
            else
                Util.mostrarToastSimple("Copia de seguridad finalizada", false);

        }
    }

    private class Restore extends AsyncTask<Object, Object, Boolean> {

        @Override
        protected void onPreExecute() {
            Util.mostrarProgreso("Restaurando backup...", true, SettingsActivity.this);
        }

        @Override
        protected Boolean doInBackground(Object... arg0) {
            return MyDatabaseTools.restaura();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            Util.mostrarProgreso("", false, null);
            if (!result)
                Util.mostrarToastSimple("No se ha podido restaurar la copia de seguridad", true);
            else
                Util.mostrarToastSimple("Restaurada la copia de seguridad", false);

        }
    }
}
